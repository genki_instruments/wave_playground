/*******************************************************************************
 The block below describes the properties of this module, and is read by
 the Projucer to automatically generate project code that uses it.
 For details about the syntax and how to create or use a module, see the
 JUCE Module Format.txt file.


 BEGIN_JUCE_MODULE_DECLARATION

  ID:               juce_ble
  vendor:           Genki Instruments
  version:          0.1
  name:             JUCE BLE
  description:      Bluetooth Low Energy module for JUCE
  website:          
  license:          

  dependencies:
  searchpaths:      ./components/include/ ./extern/gsl/include ./native/Linux/bluez
  OSXFrameworks:    CoreBluetooth
  iOSFrameworks:    
  linuxLibs:        bluetooth
  linuxPackages:    glib-2.0 gio-2.0 dbus-1
  mingwLibs:        

 END_JUCE_MODULE_DECLARATION

*******************************************************************************/

#include "ble_adapter.h"
