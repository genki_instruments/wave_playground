#ifndef BLE_PERIPHERAL_H
#define BLE_PERIPHERAL_H

#include "ble_address.h"
#include "ble_service.h"

#include <juce_core/juce_core.h>

#include <string>
#include <optional>

namespace ble {

class Peripheral
{
public:
    enum State
    {
        Unknown,
        Disconnected,
        Disconnecting,
        Connecting,
        Connected,
        ServicesResolved
    };

    class GattListener
    {
    public:
        virtual ~GattListener() = default;
        virtual void onServicesDiscovered() = 0;
        virtual void onServiceDetailsDiscovered(const Service&) = 0;
        virtual void onValueChanged(const Characteristic&, gsl::span<const gsl::byte>) = 0;
        virtual void onNotificationStateUpdated(const Characteristic&) = 0;
    };

    template<typename N>
    Peripheral(N p) : native(std::make_shared<Native>(p)) {}

    void discoverServices();

    std::optional<Service> service(const juce::Uuid& uuid) const;

    Address address() const;

    juce::String name() const;

    State state() const;

    std::vector<Service> services() const;

    int getMaximumValueLength() const;

    juce::String toString() const;

    bool operator==(const Peripheral& other) const;

    void setListener(GattListener*);

    struct Native;
    std::shared_ptr<Native> native;
};

} // namespace ble

#endif  // BLE_PERIPHERAL_H
