#ifndef BLE_SERVICE_H
#define BLE_SERVICE_H

#include <juce_core/juce_core.h>

#include <optional>
#include <gsl/span>

namespace ble {

//======================================================================================================================
// Very lightweight wrapper around a BLE Descriptor. Doesn't involve CoreBluetooth at all
class Descriptor
{
public:
    Descriptor(juce::Uuid u) : uuid(std::move(u)), handle(0) {}  // TODO: Assign handles?

    const juce::Uuid& getUuid() const { return uuid; }

    uint16_t getHandle() const { return handle; }

private:
    juce::Uuid uuid;
    uint16_t   handle;
};

//======================================================================================================================
class Characteristic
{
public:
    enum NotificationType {
        None,
        Notify,
        Indicate,
    };

    template<typename N>
    Characteristic(N n) : native(std::make_shared<Native>(n)) {}

    const Descriptor& descriptor(const juce::Uuid& uuid) const;

    std::vector<Descriptor> descriptors() const;

    juce::Uuid uuid() const;

    void write(gsl::span<const gsl::byte>, bool withResponse = true);

    struct Native;
    std::shared_ptr<Native> native;
};

//======================================================================================================================
class Service
{
public:
    template<typename N>
    Service(N n) : native(std::make_shared<Native>(n)) {}

    void discoverDetails() const;

    void writeDescriptor(const Descriptor& desc, gsl::span<const gsl::byte> data);

    void readDescriptor(const Descriptor& desc) const;

    void writeCharacteristic(const Characteristic& charact, gsl::span<const gsl::byte> data, bool withResponse = true);

    void readCharacteristic(const juce::Uuid& uuid) const;
    void readCharacteristic(const Characteristic& charact) const;

    void enableNotifications(const Characteristic& charact, Characteristic::NotificationType type = Characteristic::Notify) const;

    std::optional<Characteristic> characteristic(const juce::Uuid& uuid) const;

    std::vector<Characteristic> characteristics() const;

    juce::Uuid uuid() const;

    struct Native;
    std::shared_ptr<Native> native;
};

} // namespace ble

#endif  // BLE_SERVICE_H