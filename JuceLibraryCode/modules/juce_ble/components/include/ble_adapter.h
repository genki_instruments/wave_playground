#ifndef BLE_ADAPTER_H
#define BLE_ADAPTER_H

#include "ble_peripheral.h"

#include <juce_core/juce_core.h>

namespace ble {
class Adapter
{
public:
    enum State
    {
        Disabled,
        PoweredOff,
        Enabled,
        Discovering,
        Idle,
        Resetting,
        Unauthorized,
        Unknown,
        Unsupported,
    };

    class Listener
    {
    public:
        virtual ~Listener() = default;

        virtual void stateUpdated() = 0;
        virtual void peripheralDiscovered(const Peripheral&) = 0;
        virtual void peripheralStateUpdated(const Peripheral&) = 0;
        virtual void peripheralRssiUpdated(const Peripheral&, double rssi) = 0;
    };

    Adapter(Listener& l);

    ~Adapter();

    void addListener(Listener*);

    void removeListener(Listener*);

    void startScan(const std::vector<juce::Uuid>& uuids = {});

    void stopScan();

    void connect(const Peripheral&);

    void disconnect(Peripheral&);

    State state() const;

    std::vector<Peripheral> getConnectedPeripherals(const juce::Uuid&) const;

    struct Native;
private:
    std::unique_ptr<Native> native;
};

} // namespace ble

#endif  // BLE_ADAPTER_H
