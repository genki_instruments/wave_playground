#ifndef BLE_ADDRESS_H
#define BLE_ADDRESS_H

#ifdef JUCE_LINUX
#include "../../native/Linux/ble_address_Linux.h"
#endif

#ifdef JUCE_MAC
#include "../../native/OSX/ble_address_OSX.h"
#endif

#ifdef JUCE_WINDOWS
#include "../../native/Windows/ble_address_win.h"
#endif

#endif  // BLE_ADDRESS_H
