#ifndef BLE_ADDRESS_WIN_H
#define BLE_ADDRESS_WIN_H

#include <juce_core/juce_core.h>

namespace ble {
class Address {
public:
    Address() = default;

    Address(const juce::String& str) : addr(str) {}
    
    juce::String toString() const { return addr.toString(); }
    
    bool operator==(const Address& other) const { return addr == other.addr; }

private:
    juce::MACAddress addr;
};
}

#endif  // BLE_ADDRESS_WIN_H
