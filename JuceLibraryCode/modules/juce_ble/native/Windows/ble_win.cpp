#include "ble_peripheral.h"
#include "ble_service.h"
#include "ble_adapter.h"

#include <combaseapi.h>
#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.Storage.Streams.h>
#include <winrt/Windows.Security.Cryptography.h>
#include <winrt/Windows.Devices.Radios.h>
#include <winrt/Windows.Devices.Bluetooth.h>
#include <winrt/Windows.Devices.Bluetooth.GenericAttributeProfile.h>
#include <winrt/Windows.Devices.Enumeration.h>
#include <winrt/Windows.System.h>

using namespace winrt;
using namespace Windows::Foundation;
using namespace Windows::Devices;
using namespace Windows::Devices::Enumeration;
using namespace Windows::Devices::Radios;
using namespace Windows::Devices::Bluetooth;
using namespace Windows::Devices::Bluetooth::GenericAttributeProfile;

namespace winrt_helpers {
// TODO: There has to be a better way!

// Oh the horror
winrt::guid uuid_to_guid(const juce::Uuid& uuid)
{
    constexpr int uuid_size = 16;
    const auto* bytes = uuid.getRawData();

    const int32_t d1 = (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | (bytes[3] << 0);
    const uint16_t d2 = (bytes[4] << 8) | (bytes[5] << 0);
    const uint16_t d3 = (bytes[6] << 8) | (bytes[7] << 0);

    std::array<uint8_t, 8> d4{};
    std::copy(bytes + sizeof(d1) + sizeof(d2) + sizeof(d3), bytes + uuid_size, d4.begin());

    return winrt::guid(d1, d2, d3, d4);
}

// Oh jeez...
juce::Uuid guid_to_uuid(const winrt::guid& guid)
{
    constexpr int uuid_size = 16;
    std::array<uint8_t, uuid_size> bytes{};

    const int32_t d1 = guid.Data1;
    const uint16_t d2 = guid.Data2;
    const uint16_t d3 = guid.Data3;

    bytes[0] = (d1 >> 24) & 0xff;
    bytes[1] = (d1 >> 16) & 0xff;
    bytes[2] = (d1 >> 8) & 0xff;
    bytes[3] = (d1 >> 0) & 0xff;

    bytes[4] = (d2 >> 8) & 0xff;
    bytes[5] = (d2 >> 0) & 0xff;

    bytes[6] = (d3 >> 8) & 0xff;
    bytes[7] = (d3 >> 0) & 0xff;

    std::copy(std::cbegin(guid.Data4), std::cend(guid.Data4), bytes.begin() + sizeof(d1) + sizeof(d2) + sizeof(d3));

    return juce::Uuid(bytes.data());
}

} // namespace winrt_helpers

namespace ble {

//=======================================================================================================================
struct Characteristic::Native 
{ 
    Native(GattCharacteristic c) : characteristic(std::move(c)) {}

    GattCharacteristic characteristic;
};

void Characteristic::write(gsl::span<const gsl::byte> data, bool withResponse)
{
    using namespace Windows::Storage::Streams;

    jassert(native != nullptr);

    DataWriter writer;
    writer.ByteOrder(ByteOrder::LittleEndian);
    writer.WriteBytes({ (const uint8_t*) data.data(), (const uint8_t*) data.data() + data.size() });

    DBG("WRITING CHARACTERISTIC: " << juce::String::toHexString(data.data(), data.size()));
    native->characteristic.WriteValueWithResultAsync(writer.DetachBuffer(), withResponse ? GattWriteOption::WriteWithResponse : GattWriteOption::WriteWithoutResponse).Completed(
        [](const IAsyncOperation<GattWriteResult>& sender, AsyncStatus) 
        {
            if (sender.GetResults().Status() != GattCommunicationStatus::Success) 
            {
                DBG("ERROR WRITING CHARACTERISTIC: " << juce::String((int)sender.GetResults().Status()) 
                    << ", PROROCOL ERROR: " << juce::String((int) sender.GetResults().ProtocolError().Value()));

                DBG("ERROR WRITING CHARACTERISTIC");

                return;
            }
        }
    );
}

juce::Uuid Characteristic::uuid() const
{
    jassert(native != nullptr);

    return winrt_helpers::guid_to_uuid(native->characteristic.Uuid());
}

//===========================================================================================================================
struct Service::Native 
{ 
    Native(GattDeviceService s) : service(std::move(s))  {}

    GattDeviceService service{ nullptr };

    Peripheral::GattListener* listener = nullptr;
};

void Service::discoverDetails() const 
{ 
    jassert(native != nullptr);

    native->service.GetCharacteristicsAsync().Completed(
        [service = *this](const IAsyncOperation<GattCharacteristicsResult>& sender, AsyncStatus status)
        {
            const auto res = sender.GetResults();

            if (status != AsyncStatus::Completed || res.Status() != GattCommunicationStatus::Success)
            {
                DBG("ERROR GETTING CHARACTERISTICS");
                return;
            }

            jassert(service.native->listener != nullptr);
            service.native->listener->onServiceDetailsDiscovered(service);
        }
    );
}

juce::Uuid Service::uuid() const
{
    jassert(native != nullptr);

    return winrt_helpers::guid_to_uuid(native->service.Uuid());
}

std::vector<Characteristic> Service::characteristics() const
{
    jassert(native != nullptr);

    std::vector<Characteristic> v{};
    for (const auto& c : native->service.GetAllCharacteristics())
        v.emplace_back(c);

    return v;
}

std::optional<Characteristic> Service::characteristic(const juce::Uuid& uuid) const
{
    jassert(native != nullptr);

    const auto chars = characteristics();
    const auto it    = std::find_if(chars.cbegin(), chars.cend(), [&](const auto& c) { return c.uuid() == uuid; });

    return it == chars.cend() ? std::nullopt : std::optional<Characteristic>(*it);
}

void Service::writeDescriptor(const Descriptor&, gsl::span<const gsl::byte>) { jassert(false); }

void Service::readDescriptor(const Descriptor&) const { jassert(false); }

void Service::writeCharacteristic(const Characteristic&, gsl::span<const gsl::byte>, bool) { jassert(false); }

void Service::readCharacteristic(const Characteristic& charact) const 
{ 
    charact.native->characteristic.ReadValueAsync().Completed(
        [service = *this, charact](const IAsyncOperation<GattReadResult>& sender, AsyncStatus)
        {
            if (sender.GetResults().Status() != GattCommunicationStatus::Success) 
            {
                DBG("ERROR READING CHARACTERISTIC: " << juce::String((int)sender.GetResults().Status()));
                return;
            }

            jassert(service.native->listener != nullptr);

            const auto buf = gsl::make_span(sender.GetResults().Value().data(), sender.GetResults().Value().Length());
            service.native->listener->onValueChanged(charact, gsl::as_bytes(buf));
        }
    );
}

void Service::enableNotifications(const Characteristic& charact, Characteristic::NotificationType type) const 
{
    jassert(native != nullptr);

    const std::map<Characteristic::NotificationType, GattClientCharacteristicConfigurationDescriptorValue> types
    {
        { Characteristic::Notify, GattClientCharacteristicConfigurationDescriptorValue::Notify },
        { Characteristic::Indicate, GattClientCharacteristicConfigurationDescriptorValue::Indicate },
        { Characteristic::None, GattClientCharacteristicConfigurationDescriptorValue::None },
    };

    charact.native->characteristic.WriteClientCharacteristicConfigurationDescriptorWithResultAsync(types.at(type)).Completed(
        [service = *this, charact](const IAsyncOperation<GattWriteResult>& sender, AsyncStatus)
        {
            if (sender.GetResults().Status() != GattCommunicationStatus::Success) 
            {
                DBG("ERROR WRITING CCCD: " << juce::String((int)sender.GetResults().Status()));
                return;
            }

            charact.native->characteristic.ValueChanged([service, charact](const GattCharacteristic&, GattValueChangedEventArgs args)
            {
                jassert(service.native->listener != nullptr);

                const auto buf = gsl::make_span(args.CharacteristicValue().data(), args.CharacteristicValue().Length());
                service.native->listener->onValueChanged(charact, gsl::as_bytes(buf));
            });

            jassert(service.native->listener != nullptr);
            service.native->listener->onNotificationStateUpdated(charact);
        }
    );
}

//=====================================================================================================================
struct Peripheral::Native 
{ 
    Native(BluetoothLEDevice d) : device(std::move(d)) {}

    ~Native() 
    { 
        // NOTE: Memory might be leaking somewhere, since I don't believe that the event handler should be 
        //       called after the device/peripheral is deleted.

        // Unregister the ConnectionStatusChanged event handler that we subscribe to upon connection.
        device.ConnectionStatusChanged(token);
    }

    winrt::event_token token;
    BluetoothLEDevice device{ nullptr };

    Peripheral::GattListener* listener = nullptr;
};

// TODO
bool Peripheral::operator==(const Peripheral&) const { jassertfalse;  return false; }

void Peripheral::setListener(Peripheral::GattListener* listener) 
{ 
    jassert(native != nullptr);

    native->listener = listener;
}

Address Peripheral::address() const
{
    jassert(native != nullptr);

    // TODO: This doesn't work like intended, the string needs to be a hex string representation of the address
    return Address(juce::String(native->device.BluetoothAddress()));
}

juce::String Peripheral::name() const
{ 
    jassert(native != nullptr);

    return juce::String(native->device.Name().c_str()); 
}

void Peripheral::discoverServices()
{
    using namespace Windows::Devices::Bluetooth::GenericAttributeProfile;

    jassert(native != nullptr);

    native->device.GetGattServicesAsync().Completed(
        [this](const IAsyncOperation<GattDeviceServicesResult>&, AsyncStatus status)
        {
            if (status != AsyncStatus::Completed) 
            {
                DBG("ERROR GETTING SERVICES");
                return;
            }

            jassert(native->listener != nullptr);
            native->listener->onServicesDiscovered();
        }
    );
}

Peripheral::State Peripheral::state() const
{
    const std::map<BluetoothConnectionStatus, Peripheral::State> states
    {
        {BluetoothConnectionStatus::Disconnected,  State::Disconnected},
        {BluetoothConnectionStatus::Connected,  State::Connected},
    };

    jassert(native != nullptr);

    return states.at(native->device.ConnectionStatus());
}

std::vector<Service> Peripheral::services() const
{
    jassert(native != nullptr);

    const auto services = native->device.GetGattServicesAsync().get();

    std::vector<Service> v{};
    for (const auto& s : services.Services())
        v.push_back(s);

    return v;
}

std::optional<Service> Peripheral::service(const juce::Uuid& uuid) const
{
    jassert(native != nullptr);

    try {
        Service::Native service(native->device.GetGattService(winrt_helpers::uuid_to_guid(uuid)));
        service.listener = native->listener;

        return std::optional<Service>(service);

    } catch (const winrt::hresult_error& e) {
        DBG("Exception while getting service for uuid: " << uuid.toDashedString() << " - " << e.message().c_str());
    }

    return std::nullopt;
}

int Peripheral::getMaximumValueLength() const
{
    jassert(native != nullptr);

    return GattSession::FromDeviceIdAsync(native->device.BluetoothDeviceId()).get().MaxPduSize() - 3;
}

//===========================================================================================================================
struct Adapter::Native
{
public:
    Native(Adapter::Listener& l);

    ~Native();

    Adapter::Listener& listener;

    DeviceWatcher watcher{ nullptr };
    Radio radio{ nullptr };
};

//===========================================================================================================================
Adapter::Adapter(Adapter::Listener& l) : native(std::make_unique<Adapter::Native>(l)) { }

Adapter::~Adapter() {}

std::vector<Peripheral> Adapter::getConnectedPeripherals(const juce::Uuid&) const
{
    const auto collection = DeviceInformation::FindAllAsync(BluetoothLEDevice::GetDeviceSelectorFromConnectionStatus(BluetoothConnectionStatus::Connected)).get();

    std::vector<Peripheral> v;
    for (const auto& d : collection) {
        try {
            v.emplace_back(BluetoothLEDevice::FromIdAsync(d.Id()).get());
        }
        catch (const winrt::hresult_error& e) {
            DBG("Error creating BluetoothLEDevice: " << d.Id().c_str() << ": " << e.message().c_str());
        }
    }

    return v;
}

void Adapter::startScan(const std::vector<juce::Uuid>&) 
{ 
    DBG("BLE Adapter starting scan");

    // Additional properties we would like about the device.
    // Property strings are documented here https://msdn.microsoft.com/en-us/library/windows/desktop/ff521659(v=vs.85).aspx
    auto requestedProperties = single_threaded_vector<hstring>({ L"System.Devices.Aep.DeviceAddress", L"System.Devices.Aep.IsConnected", L"System.Devices.Aep.Bluetooth.Le.IsConnectable" });

    // BT_Code: Example showing paired and non-paired in a single query.
    hstring aqsAllBluetoothLEDevices = L"(System.Devices.Aep.ProtocolId:=\"{bb7bb05e-5972-42b5-94fc-76eaa7084d49}\")";

    native->watcher =
        Windows::Devices::Enumeration::DeviceInformation::CreateWatcher(
            aqsAllBluetoothLEDevices,
            requestedProperties,
            DeviceInformationKind::AssociationEndpoint);

    using namespace Windows::Devices::Enumeration;

    // Register event handlers before starting the watcher.
    native->watcher.Added(
        [&](const DeviceWatcher&, DeviceInformation deviceInfo)
        {
            const juce::String name(deviceInfo.Name().c_str());
            const juce::String id(deviceInfo.Id().c_str());
            const bool is_paired = deviceInfo.Pairing().IsPaired();

            DBG("DEVICE ADDED: " << name << " - " << id << ", is paired? " << (is_paired ? "YES" : "NO"));

            // We might hit peripherals that are paired, but not available.
            const auto props = deviceInfo.Properties();
            if (props.Lookup(L"System.Devices.Aep.Bluetooth.Le.IsConnectable")) {
                native->listener.peripheralDiscovered({BluetoothLEDevice::FromIdAsync(deviceInfo.Id()).get()});
            }
        }
    );

    native->watcher.Updated( [](const DeviceWatcher&, DeviceInformationUpdate) { } );
    native->watcher.Removed( [](const DeviceWatcher&, DeviceInformationUpdate) { } );
    native->watcher.EnumerationCompleted( [](const DeviceWatcher&, Windows::Foundation::IInspectable const&) { } );
    native->watcher.Stopped( [](const DeviceWatcher&, Windows::Foundation::IInspectable const&) { } );

    // Start the watcher. Active enumeration is limited to approximately 30 seconds.
    // This limits power usage and reduces interference with other Bluetooth activities.
    // To monitor for the presence of Bluetooth LE devices for an extended period,
    // use the BluetoothLEAdvertisementWatcher runtime class. See the BluetoothAdvertisement
    // sample for an example.
    native->watcher.Start();
}

void Adapter::stopScan() 
{ 
    if (native->watcher.Status() == DeviceWatcherStatus::Started)
        native->watcher.Stop(); 
}

void Adapter::connect(const Peripheral& peripheral) 
{
    DBG("CONNECTING TO PERIPHERAL: " << peripheral.name());

    // NOTE: Something very peculiar is happening here. The event handler seems to be called AFTER
    //       both the peripheral and the listener are deleted. Are we leaking memory somewhere?
    //       This token is used to unregister the ConnectionStatusChanged event handler when the peripheral is deleted.
    peripheral.native->token = peripheral.native->device.ConnectionStatusChanged( [this](const BluetoothLEDevice& sender, IInspectable obj)  
    {
        DBG("PERIPHERAL CONNECTION STATUS CHANGED: " << juce::String(sender.Name().c_str()) << " - " << juce::String((int) sender.ConnectionStatus()));

        native->listener.peripheralStateUpdated({ sender });
    });

    if (peripheral.state() == Peripheral::Connected) {
        DeviceAccessStatus status = peripheral.native->device.RequestAccessAsync().get();
        if (status != DeviceAccessStatus::Allowed) {
            DBG("ERROR GETTING ACCESS TO DEVICE: " << juce::String((int)status));
            return;
        }

        native->listener.peripheralStateUpdated(peripheral);
    } else {
        // NOTE: We need to interact with the peripheral in some way to get it into the "Connected" state
        //       This is one way to achieve that...
        peripheral.native->device.GetGattServicesAsync();
    }
}

Adapter::State Adapter::state() const
{
    const std::map<RadioState, State> states{
        {RadioState::Disabled, State::Disabled},
        {RadioState::Off, State::PoweredOff},
        {RadioState::On, State::Idle},
        {RadioState::Unknown, State::Unknown},
    };

    if (const auto it = states.find(native->radio.State()); it != states.cend())
        return it->second;


    return State::Unknown; 
}

Adapter::Native::Native(Adapter::Listener& l) : listener(l) 
{ 
    BluetoothAdapter::GetDefaultAsync().Completed(
        [native = this](const IAsyncOperation<BluetoothAdapter>& adapter, AsyncStatus status)
        {
            if (status != AsyncStatus::Completed)
                return;

            adapter.GetResults().GetRadioAsync().Completed(
                [native](const IAsyncOperation<Radio>& radio, AsyncStatus status)
                {
                    if (status != AsyncStatus::Completed)
                        return;

                    native->radio = radio.GetResults();
                    native->radio.StateChanged([native](auto, auto) { native->listener.stateUpdated(); });

                    native->listener.stateUpdated();
                }
            );
        }
    );
}

Adapter::Native::~Native() { }

} // namespace ble
