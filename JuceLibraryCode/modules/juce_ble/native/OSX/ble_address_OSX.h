#ifndef BLE_ADDRESS_OSX_H
#define BLE_ADDRESS_OSX_H

#include <juce_core/juce_core.h>

namespace ble {
class Address {
public:
    Address() = default;

    Address(const juce::String& str) : uuid(str) {}

    [[nodiscard]] juce::String toString() const {return uuid.toDashedString(); }

    bool operator==(const Address& other) const { return uuid == other.uuid; }

private:
    const juce::Uuid uuid;
};
}

#endif  // BLE_ADDRESS_LINUX_H
