//#ifndef BLE_ADAPTER_OSX_H
//#define BLE_ADAPTER_OSX_H
//
//#include "ble_adapter.h"
//#include "ble_peripheral_OSX.h"
//
//#import <Foundation/Foundation.h>
//#import <CoreBluetooth/CoreBluetooth.h>
//
//@interface OSXBluetoothConnection : NSObject {
//    BleAdapterNative* cpp_bleAdapterNative;
//    BleAdapter::Delegate* cpp_delegate;
//}
//@end
//
//class BleAdapterOSX: public BleAdapterNative {
//private:
//    OSXBluetoothConnection* m_bluetooth;
//
//public:
//    BleAdapterOSX(BleAdapter::Delegate& delegate);
//
//    ~BleAdapterOSX();
//
//    void start() override;
//    std::vector<BlePeripheral*> getConnectedPeripherals(const juce::Uuid&) override;
//    void scan(const std::vector<juce::Uuid>& uuids) override;
//    void stopScan() override;
//    void connect(BleAddress address) override;
//    void connect(BlePeripheral& peripheral) override;
//    void disconnect(BlePeripheral& peripheral) override;
//
//    void readDescriptor(const BleDescriptor& desc) override;
//    void writeDescriptor(const BleDescriptor& desc, const ByteArray& data) override;
//
//    void readCharacteristic(const BleCharacteristic& charact) override;
//    void writeCharacteristic(const BleCharacteristic& charact, const ByteArray& data) override;
//
//    void enableNotifications(const BleCharacteristic& charact, bool enable) override;
//
//    void cbAdapterStateUpdated(CBCentralManager* central);
//    void cbPeripheralStateUpdated(CBPeripheral* peripheral, BlePeripheral::State state);
//    void cbPeripheralDiscovered(CBPeripheral* peripheral);
//    void cbServiceDiscovered(CBService* service);
//    void cbCharacteristicDiscovered(CBCharacteristic* characteristic);
//    void cbServiceDiscoveryComplete(CBPeripheral* peripheral);
//    void cbServiceDetailsDiscoveryComplete(CBPeripheral* peripheral, CBService* service);
//    void cbCharacteristicValueUpdated(CBPeripheral* peripheral, CBCharacteristic* characteristic);
//    void cbNotificationStateUpdated(CBCharacteristic* characteristic);
//
//    juce::Uuid uuidFromCBUUID(CBUUID* uuid);
//
//    BlePeripheral& peripheralFromCbPeripheral(CBPeripheral* cb) const;
//};
//
//#endif  // BLE_ADAPTER_OSX_H
