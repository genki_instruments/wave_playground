#include "ble_peripheral.h"
#include "ble_service.h"
#include "ble_adapter.h"

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

namespace ble {
namespace cb_helpers {
juce::Uuid convert_uuid(const CBUUID* _Nonnull uuid)
{
    const NSString* str = [uuid UUIDString];

    // Standard Bluetooth UUIDs are just represented by their 16-bit UUID.
    // Need to pad these with the 128-bit base UUID.
    if ([str length] == 4)
    {
        juce::String jstr = juce::String("0000") + juce::String([str UTF8String]).toLowerCase() + juce::String("-0000-1000-8000-00805f9b34fb");

        return juce::Uuid(jstr);
    }
    else
    {
        return juce::Uuid([[uuid UUIDString] UTF8String]);
    }
}

CBUUID* convert_juce_uuid(const juce::Uuid& uuid)
{
    NSString* uuid_str = [NSString stringWithUTF8String:uuid.toDashedString().toStdString().c_str()];
    return [CBUUID UUIDWithString:uuid_str];
}
} // namespace cb_helpers

struct Characteristic::Native
{
    Native(CBCharacteristic* _Nonnull c) : characteristic(c) { [characteristic retain]; }

    ~Native() { [characteristic release]; }

    // Reference counted by the Objective-C runtime.
    CBCharacteristic* _Nonnull characteristic;
};

void Characteristic::write(gsl::span<const gsl::byte> data, bool withResponse)
{
    jassert(native != nullptr);

    DBG("Writing characteristic: " << uuid().toDashedString() << ", data: " << juce::String::toHexString(data.data(), data.size()));

    NSData* ns_data = [NSData dataWithBytes:data.data() length:(unsigned long) data.size()];
    const auto type = withResponse ? CBCharacteristicWriteWithResponse : CBCharacteristicWriteWithoutResponse;
    [native->characteristic.service.peripheral writeValue:ns_data forCharacteristic:native->characteristic type:type];
}

juce::Uuid Characteristic::uuid() const
{
    jassert(native != nullptr);

    return {cb_helpers::convert_uuid(native->characteristic.UUID)};
}

struct Service::Native
{
    Native(CBService* _Nonnull c) : service(c) { [service retain]; }

    ~Native() { [service release]; }

    // Reference counted by the Objective-C runtime.
    CBService* _Nonnull service;
};

void Service::discoverDetails() const
{
    jassert(native != nullptr);

    [native->service.peripheral discoverCharacteristics:nil forService:native->service];
}

juce::Uuid Service::uuid() const
{
    jassert(native != nullptr);

    return {cb_helpers::convert_uuid(native->service.UUID)};
}

std::vector<Characteristic> Service::characteristics() const
{
    NSArray<CBCharacteristic*>* cb_chars = [native->service characteristics];

    // TODO: iterator based? for_each?
    std::vector<Characteristic> chars{};

    for (unsigned int i = 0; i < [cb_chars count]; ++i)
    {
        chars.emplace_back(cb_chars[i]);
    }

    return chars;
}

std::optional<Characteristic> Service::characteristic(const juce::Uuid& uuid) const
{
    const auto chars = characteristics();
    const auto it    = std::find_if(chars.cbegin(), chars.cend(), [&](const auto& c) { return c.uuid() == uuid; });

    return it == chars.cend() ? std::nullopt : std::optional<Characteristic>(*it);
}

void Service::writeDescriptor(const Descriptor&, gsl::span<const gsl::byte>)
{
//    jassert(native != nullptr);
//
//    const auto it = std::find_if(native->descriptors.cbegin(), native->descriptors.cend(),
//            [&](const CBDescriptor* d) { return desc.getUuid() == cb_helpers::convert_uuid(d.UUID); }
//    );
//
//    jassert(it != native->descriptors.cend());
//
//    NSData* ns_data = [NSData dataWithBytes:data.base() length:data.length()];
//    [native->service.peripheral writeValue:ns_data forDescriptor:(*it)];
}

void Service::readDescriptor(const Descriptor&) const
{
//    jassert(native != nullptr);
//
//    const auto it = std::find_if(native->descriptors.cbegin(), native->descriptors.cend(),
//            [&](const CBDescriptor* d) { return desc.getUuid() == cb_helpers::convert_uuid(d.UUID); }
//    );
//
//    jassert(it != native->descriptors.cend());
//
//    [native->service.peripheral readValueForDescriptor:(*it)];
}

void Service::writeCharacteristic(const Characteristic& charact, gsl::span<const gsl::byte> data, bool withResponse)
{
    jassert(native != nullptr);

    NSData* ns_data = [NSData dataWithBytes:data.data() length:(unsigned long) data.size()];
    const auto type = withResponse ? CBCharacteristicWriteWithResponse : CBCharacteristicWriteWithoutResponse;
    [native->service.peripheral writeValue:ns_data forCharacteristic:charact.native->characteristic type:type];
}

void Service::readCharacteristic(const juce::Uuid& uuid) const
{
    if (auto c = characteristic(uuid); c != std::nullopt)
        readCharacteristic(*c);
}

void Service::readCharacteristic(const Characteristic& charact) const
{
    jassert(native != nullptr);

    [native->service.peripheral readValueForCharacteristic:charact.native->characteristic];
}

void Service::enableNotifications(const Characteristic& charact, Characteristic::NotificationType type) const
{
    jassert(native != nullptr);

    const bool disable = type == Characteristic::None;
    [native->service.peripheral setNotifyValue:!disable forCharacteristic:charact.native->characteristic];
}

} // namespace ble

//======================================================================================================================
@interface OSXPeripheralDelegate : NSObject
{
    ble::Peripheral::GattListener* listener;
}
@end

@interface OSXPeripheralDelegate () <CBPeripheralDelegate>
@end

@implementation OSXPeripheralDelegate
- (nonnull instancetype)initWithPeripheralListener:(nonnull CBPeripheral*)peripheral list:(nullable ble::Peripheral::GattListener*)list {
    self = [super init];

    if (self)
    {
        listener = list;

        [peripheral setDelegate:self];
    }

    return self;
}

- (void)peripheral:(nonnull CBPeripheral*)peripheral didUpdateNotificationStateForCharacteristic:(nonnull CBCharacteristic*)characteristic error:(nullable NSError*)error {
    juce::ignoreUnused(peripheral);

    if (error)
    {
        DBG("<OSX> Error updating notification state: " << [error.localizedDescription UTF8String]);
        return;
    }

    DBG(juce::String([[characteristic.UUID UUIDString] UTF8String]));

    if (listener != nullptr)
        listener->onNotificationStateUpdated({characteristic});
}

- (void)peripheral:(nonnull CBPeripheral*)peripheral didDiscoverServices:(nullable NSError*)error {
    if (error)
    {
        DBG("<OSX> Error discovering services: " << [error.localizedDescription UTF8String]);
        return;
    }

    if (listener == nullptr)
        return;

    for (CBService* service in peripheral.services)
        listener->onServiceDetailsDiscovered({service});

    listener->onServicesDiscovered();
}

- (void)peripheral:(nonnull CBPeripheral*)peripheral didDiscoverCharacteristicsForService:(nonnull CBService*)service error:(nullable NSError*)error {
    juce::ignoreUnused(peripheral);

    if (error)
    {
        DBG("<OSX> Error discovering service characteristics: " << [error.localizedDescription UTF8String]);
        return;
    }

    if (listener != nullptr)
        listener->onServiceDetailsDiscovered({service});
}

- (void)peripheral:(nonnull CBPeripheral*)peripheral didUpdateValueForCharacteristic:(nonnull CBCharacteristic*)characteristic error:(nullable NSError*)error {
    juce::ignoreUnused(peripheral);
    juce::ignoreUnused(error);

    const NSData* ns_data = [characteristic value];
    const NSUInteger len = ns_data.length;
    const unsigned char* bytes = (unsigned char*) [ns_data bytes];

    listener->onValueChanged({characteristic}, gsl::as_bytes(gsl::make_span(bytes, (long) len)));
}

@end

namespace ble {
struct Peripheral::Native
{
    Native(CBPeripheral* _Nonnull c) : peripheral(c), delegate(nullptr)
    {
        jassert(c != nullptr);

        [peripheral retain];
    }

    ~Native()
    {
        [peripheral release];

        if (delegate != nullptr)
            [delegate release];
    }

    // Reference counted by the Objective-C runtime.
    CBPeripheral* _Nonnull peripheral;

    // TODO: What about this? Is this automatically released?
    OSXPeripheralDelegate* _Nullable delegate;
};

bool Peripheral::operator==(const Peripheral& other) const
{
    return native != nullptr && other.native != nullptr && native->peripheral == other.native->peripheral;
}

void Peripheral::setListener(Peripheral::GattListener* _Nullable listener)
{
    jassert(native != nullptr);

    native->delegate = [[OSXPeripheralDelegate alloc] initWithPeripheralListener:native->peripheral list:listener];
}

Address Peripheral::address() const
{
    jassert(native != nullptr);

    // The warning here doesn't seem to be correct. The identifier property is indeed available on macOS 10.12
    return {juce::String([native->peripheral.identifier.UUIDString UTF8String])};
}

juce::String Peripheral::name() const
{
    jassert(native != nullptr);

    const NSString* name = native->peripheral.name;
    return name == nullptr ? "" : [name UTF8String];
}

void Peripheral::discoverServices()
{
    jassert(native != nullptr);

    [native->peripheral discoverServices:nil];
}

Peripheral::State Peripheral::state() const
{
    const std::map<NSInteger, Peripheral::State> states
                                                         {
                                                                 {CBPeripheralStateDisconnected, State::Disconnected},
                                                                 {CBPeripheralStateConnecting,   State::Connecting},
                                                                 {CBPeripheralStateConnected,    State::Connected},
                                                         };

    if (const auto it = states.find(native->peripheral.state); it != states.cend())
        return it->second;

    return State::Unknown;
}

std::vector<Service> Peripheral::services() const
{
    const NSArray<CBService*>* cb_services = [native->peripheral services];

    std::vector<Service> services{};
    services.reserve(cb_services.count);

    for (unsigned int i = 0; i < [cb_services count]; ++i)
        services.emplace_back(cb_services[i]);

    return services;
}

std::optional<Service> Peripheral::service(const juce::Uuid& uuid) const
{
    const auto srv = services();
    const auto it  = std::find_if(srv.cbegin(), srv.cend(), [&](const auto& s) { return s.uuid() == uuid; });

    return it == srv.cend() ? std::nullopt : std::optional<Service>(*it);
}

int Peripheral::getMaximumValueLength() const
{
    jassert(native != nullptr);

    return [native->peripheral maximumWriteValueLengthForType:CBCharacteristicWriteType::CBCharacteristicWriteWithoutResponse];
}

} // namespace ble

//======================================================================================================================
@interface OSXAdapter : NSObject
{
    std::vector<ble::Adapter::Listener*> adapterListeners;
}
@end

@interface OSXAdapter () <CBCentralManagerDelegate>
@property(nonatomic, strong) CBCentralManager* _Nullable centralManager;
@end

namespace ble {
struct Adapter::Native
{
public:
    explicit Native(Adapter::Listener& l);

    ~Native();

    OSXAdapter* _Nullable adapter;
};

} // namespace ble

@implementation OSXAdapter

- (nonnull instancetype)initWithDelegate:(nullable ble::Adapter::Listener*)delegate {
    self = [super init];

    if (self)
        adapterListeners.push_back(delegate);

    return self;
}

- (void)setup {
    DBG("Setting up Central Manager");

    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
}

- (void)addListener:(ble::Adapter::Listener*) listener {
    const auto pred = [listener](const ble::Adapter::Listener* l) { return l == listener; };

    if (std::none_of(adapterListeners.cbegin(), adapterListeners.cend(), pred))
        adapterListeners.push_back(listener);

    std::printf("Adapter listeners:\n");
    for (const auto* l : adapterListeners)
        std::printf("%p\n", (long) l);
}

- (void)removeListener:(ble::Adapter::Listener*) listener {
    auto it = std::remove(adapterListeners.begin(), adapterListeners.end(), listener);
    adapterListeners.erase(it, adapterListeners.end());
}

- (void)centralManager:(nonnull CBCentralManager*)central didConnectPeripheral:(nonnull CBPeripheral*)peripheral {
    juce::ignoreUnused(central);

    // For some reason the callback is sometimes fired twice. Need to do this check.
    const ble::Peripheral p(peripheral);

    DBG("didConnectPeripheral: " << p.name());

    for (auto* l : adapterListeners)
        l->peripheralStateUpdated(p);
}

- (void)centralManager:(nonnull CBCentralManager*)central didFailToConnectPeripheral:(nullable NSError*)error {
    juce::ignoreUnused(central);
    juce::ignoreUnused(error);

    DBG("Failed to connect: " << [error.localizedDescription UTF8String]);

    // TODO: dealloc and invalidate peripheral delegate object
}

- (void)centralManager:(nonnull CBCentralManager*)central didDisconnectPeripheral:(nonnull CBPeripheral*)peripheral error:(nullable NSError*)error {
    juce::ignoreUnused(central);
    juce::ignoreUnused(error);

    const ble::Peripheral p(peripheral);
    DBG("didDisconnectPeripheral: " << p.name());

    for (auto* l : adapterListeners)
        l->peripheralStateUpdated(p);
}

- (void)centralManager:(nonnull CBCentralManager*)central didDiscoverPeripheral:(nonnull CBPeripheral*)peripheral
     advertisementData:(nonnull NSDictionary<NSString*, id>*)advertisementData
                  RSSI:(nonnull NSNumber*)RSSI {
    juce::ignoreUnused(central);
    juce::ignoreUnused(advertisementData);
    juce::ignoreUnused(RSSI);

    for (auto* l : adapterListeners)
        l->peripheralDiscovered({peripheral});
}

- (void)centralManagerDidUpdateState:(nonnull CBCentralManager*)central {
    juce::ignoreUnused(central);

    for (auto* l : adapterListeners)
        l->stateUpdated();
}

@end

namespace ble {
Adapter::Adapter(Adapter::Listener& l) : native(std::make_unique<Adapter::Native>(l))
{
    [native->adapter setup];
}

Adapter::~Adapter() = default;

void Adapter::addListener(Listener* l) { [native->adapter addListener:l]; }

void Adapter::removeListener(Listener* l) { [native->adapter removeListener:l]; }

std::vector<Peripheral> Adapter::getConnectedPeripherals(const juce::Uuid& uuid) const
{
    NSString* uuid_str = [NSString stringWithUTF8String:uuid.toDashedString().toStdString().c_str()];
    NSArray * uuids    = @[[CBUUID UUIDWithString:uuid_str]];
    NSArray<CBPeripheral*>* peripherals = [[native->adapter centralManager] retrieveConnectedPeripheralsWithServices:uuids];

    std::vector<Peripheral> v{};
    v.reserve(peripherals.count);

    for (unsigned int i = 0; i < [peripherals count]; ++i)
    {
        DBG([peripherals[i].name UTF8String]);

        v.emplace_back(peripherals[i]);
    };

    return v;
}

void Adapter::startScan(const std::vector<juce::Uuid>& uuids)
{
    DBG("<OSX> Starting BLE scan");

    if (uuids.size() > 0)
    {
        std::vector<CBUUID*> cb_uuids(uuids.size());
        std::transform(uuids.cbegin(), uuids.cend(), cb_uuids.begin(), cb_helpers::convert_juce_uuid);

        NSArray* ns_uuids = [NSArray arrayWithObjects:&cb_uuids[0] count:cb_uuids.size()];

        [[native->adapter centralManager] scanForPeripheralsWithServices:ns_uuids options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @(NO)}];
    }
    else
    {
        [[native->adapter centralManager] scanForPeripheralsWithServices:nil options:@{CBCentralManagerScanOptionAllowDuplicatesKey: @(NO)}];
    }
}

void Adapter::stopScan()
{
    [[native->adapter centralManager] stopScan];
}

void Adapter::connect(const Peripheral& peripheral)
{
    DBG("<OSX> Connecting to: " << peripheral.name());

    [[native->adapter centralManager] connectPeripheral:peripheral.native->peripheral options:nil];
}

void Adapter::disconnect(Peripheral& peripheral)
{
    DBG("<OSX> Disconnecting peripheral: " << peripheral.address().toString());

    [[native->adapter centralManager] cancelPeripheralConnection:peripheral.native->peripheral];
}

Adapter::State Adapter::state() const
{
    // The enum constants for CBManagerState... and (deprecated) CBCentralManagerState... are the same.
    // Wrapping these in in if (@available...) was potentially causing issues on macOS 10.12
    const std::map<NSInteger, Adapter::State> states
                                                      {
                                                              {CBManagerStatePoweredOff,   State::PoweredOff},
                                                              {CBManagerStatePoweredOn,    State::Idle},
                                                              {CBManagerStateResetting,    State::Resetting},
                                                              {CBManagerStateUnauthorized, State::Unauthorized},
                                                              {CBManagerStateUnknown,      State::Unknown},
                                                              {CBManagerStateUnsupported,  State::Unsupported},
                                                      };

    return states.at([[native->adapter centralManager] state]);
}

Adapter::Native::Native(Adapter::Listener& l)
{
    adapter = [[OSXAdapter alloc] initWithDelegate:&l];
}

Adapter::Native::~Native()
{
    [adapter release];
}

} // namespace ble
