//
// Created by horigome on 27.8.2019.
//

#include "ble_peripheral.h"
#include "ble_service.h"
#include "ble_adapter.h"

extern "C" {
// These C files have some variables named  "class"...
#define class _class
#include "bluez/gdbus/gdbus.h"
#include "bluez/src/dbus-common.h"
#undef class
}

#include <utility>
#include <string_view>

namespace ble {
namespace dbus {

template<typename T>
T getProperty(GDBusProxy* proxy, std::string_view name)
{
    if (proxy != nullptr)
    {
        DBusMessageIter iter{};
        T               prop{};

        if (g_dbus_proxy_get_property(proxy, name.data(), &iter))
        {
            dbus_message_iter_get_basic(&iter, &prop);
            return prop;
        }
    }

    jassertfalse;
    return {};
}

struct Error
{
    Error() { dbus_error_init(&error); }
    Error(Error&& other) noexcept
    {
        error.name    = std::exchange(other.error.name, nullptr);
        error.message = std::exchange(other.error.message, nullptr);
    }

    ~Error() { dbus_error_free(&error); }

    DBusError& get() { return error; }
    const DBusError& get() const { return error; }

private:
    DBusError error{};
};

std::optional<Error> getError(DBusMessage* message)
{
    Error error{};
    if (dbus_set_error_from_message(&error.get(), message))
        return std::move(error);

    return std::nullopt;
}

struct Proxy
{
    Proxy(GDBusClient* client, std::string_view path, std::string_view interface)
    {
        proxy = g_dbus_proxy_new(client, path.data(), interface.data());
    }

    ~Proxy() { g_dbus_proxy_unref(proxy); }

    GDBusProxy* get() { return proxy; }

    std::string_view path() const { return g_dbus_proxy_get_path(proxy); }

private:
    GDBusProxy* proxy = nullptr;
};

std::string_view getObjectPath(GDBusProxy* proxy) { return g_dbus_proxy_get_path(proxy); };

} // namespace dbus

//=======================================================================================================================
struct Characteristic::Native
{
    Native(GDBusProxy* p) : proxy(p) {}

    GDBusProxy* proxy;
};

void Characteristic::write(gsl::span<const gsl::byte> data, bool withResponse)
{
    jassert(native != nullptr);

    struct Args
    {
        const gsl::span<const gsl::byte> payload;
        const bool                       with_response;
    };

    const auto setup = [](DBusMessageIter* iter, void* user_data)
    {
        jassert(user_data != nullptr);

        const auto* args = static_cast<Args*>(user_data);

        DBusMessageIter array{}, dict{};

        // NOTE: The D-Bus API is expeecting a double-pointer...
        const auto* b = args->payload.data();

        dbus_message_iter_open_container(iter, DBUS_TYPE_ARRAY, "y", &array);
        dbus_message_iter_append_fixed_array(&array, DBUS_TYPE_BYTE, &b, (int) args->payload.size());
        dbus_message_iter_close_container(iter, &array);

        dbus_message_iter_open_container(
                iter, DBUS_TYPE_ARRAY,
                DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING
                DBUS_TYPE_STRING_AS_STRING
                DBUS_TYPE_VARIANT_AS_STRING
                DBUS_DICT_ENTRY_END_CHAR_AS_STRING,
                &dict
        );

        const char* type = (args->with_response ? "request" : "command");
        g_dbus_dict_append_entry(&dict, "type", DBUS_TYPE_STRING, &type);

        dbus_message_iter_close_container(iter, &dict);
    };

    const auto reply = [](DBusMessage* message, void*)
    {
        if (const auto err = dbus::getError(message); err.has_value())
        {
            DBG("Failed to write: " << err->get().name << ", reason: " << err->get().message);
            return;
        }
    };

    const Args args{.payload = data, .with_response = withResponse};
    if (!g_dbus_proxy_method_call(native->proxy, "WriteValue", setup, reply, (void*) &args, nullptr))
            DBG("D-Bus method call failed: WriteValue");
}

juce::Uuid Characteristic::uuid() const
{
    jassert(native != nullptr);

    return juce::String(dbus::getProperty<const char*>(native->proxy, "UUID"));
}

//===========================================================================================================================
struct Service::Native
{
    Native(GDBusProxy* p, std::shared_ptr<Peripheral::Native> periph)
            : proxy(p),
              peripheral(periph) {}

    GDBusProxy* proxy;

    std::map<juce::Uuid, Characteristic> characteristics;
    std::weak_ptr<Peripheral::Native>    peripheral;
};

struct Peripheral::Native
{
    Native(GDBusProxy* p) : proxy(p) {}

    GDBusProxy* proxy;

    GattListener* listener = nullptr;

    std::map<juce::Uuid, Service> services;
};

void Service::discoverDetails() const
{
    // There is no specific action to take with the BlueZ D-Bus interface.
    // When services for a specific peripheral are resolved (upon connection),
    // the corresponding characteristics and other details are
    // automatically resolved.

    if (auto p = native->peripheral.lock())
        if (p->listener != nullptr)
            p->listener->onServiceDetailsDiscovered(*this);
}

juce::Uuid Service::uuid() const
{
    jassert(native != nullptr);

    return juce::String(dbus::getProperty<const char*>(native->proxy, "UUID"));
}

std::vector<Characteristic> Service::characteristics() const
{
    jassert(native != nullptr);

    std::vector<Characteristic> v{};
    v.reserve(native->characteristics.size());

    for (const auto& c : native->characteristics)
        v.push_back(c.second);

    return v;
}

std::optional<Characteristic> Service::characteristic(const juce::Uuid& uuid) const
{
    jassert(native != nullptr);

    if (const auto it = native->characteristics.find(uuid); it != native->characteristics.cend())
        return it->second;

    return std::nullopt;
}

void Service::writeDescriptor(const Descriptor&, gsl::span<const gsl::byte>) { jassertfalse; }

void Service::readDescriptor(const Descriptor&) const { jassertfalse; }

void Service::writeCharacteristic(const Characteristic& charact, gsl::span<const gsl::byte> data, bool withResponse)
{
    // TODO: Can Characteristic::write() be const? If not, charact shouldn't be a const reference
    const_cast<Characteristic&>(charact).write(data, withResponse);
}

void Service::readCharacteristic(const Characteristic& charact) const
{
    // TODO: Should this just be Characteristic::read() ?

    struct Args
    {
        gsl::span<const gsl::byte> data;
        GDBusProxy* proxy;
    };

    const auto setup = [](DBusMessageIter* iter, void*)
    {
        // TODO: What does this do?
        DBusMessageIter dict{};

        dbus_message_iter_open_container(
                iter, DBUS_TYPE_ARRAY,
                DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING
                DBUS_TYPE_STRING_AS_STRING
                DBUS_TYPE_VARIANT_AS_STRING
                DBUS_DICT_ENTRY_END_CHAR_AS_STRING,
                &dict
        );

        dbus_message_iter_close_container(iter, &dict);
    };

    const auto reply = [](DBusMessage* message, void* user_data)
    {
        if (const auto err = dbus::getError(message); err.has_value())
        {
            DBG("Failed to read: " << err->get().name);
            return;
        }
    };

    Args args{{}, charact.native->proxy};

    if (!g_dbus_proxy_method_call(charact.native->proxy, "ReadValue", setup, reply, &args, nullptr))
            DBG("D-Bus method call failed: ReadValue");
}

void Service::enableNotifications(const Characteristic& charact, Characteristic::NotificationType type) const
{
    jassert(native != nullptr);

    const bool should_enable = type == Characteristic::Notify || type == Characteristic::Indicate;
    const char* method = (should_enable) ? "StartNotify" : "StopNotify";

    const auto reply = [](DBusMessage* message, void* user_data)
    {
        if (const auto err = dbus::getError(message); err.has_value())
        {
            const auto enable = (bool) GPOINTER_TO_UINT(user_data);

            DBG("Failed to " << (enable ? "enable" : "disable") << " notifications: " << err->get().name);
            return;
        }
    };

    if (!g_dbus_proxy_method_call(charact.native->proxy, method, nullptr, reply, GUINT_TO_POINTER(should_enable), nullptr))
            DBG("D-Bus method call failed: " << method);
}

//=====================================================================================================================
bool Peripheral::operator==(const Peripheral& other) const
{
    jassert(native != nullptr);

    return native->proxy == other.native->proxy;
}

void Peripheral::setListener(Peripheral::GattListener* l)
{
    jassert(native != nullptr);

    native->listener = l;
}

Address Peripheral::address() const
{
    jassert(native != nullptr);

    return Address(dbus::getProperty<const char*>(native->proxy, "Address"));
}

juce::String Peripheral::name() const
{
    jassert(native != nullptr);

    return dbus::getProperty<const char*>(native->proxy, "Alias");
}

void Peripheral::discoverServices()
{
    jassert(native != nullptr);

    // The ServicesResolved proerty will eventually change, and trigger the callback
    if (!dbus::getProperty<bool>(native->proxy, "ServicesResolved"))
        return;

    if (native->listener != nullptr)
        native->listener->onServicesDiscovered();
}

Peripheral::State Peripheral::state() const
{
    jassert(native != nullptr);

    return dbus::getProperty<bool>(native->proxy, "ServicesResolved") ? State::Connected : State::Disconnected;
}

std::vector<Service> Peripheral::services() const
{
    jassert(native != nullptr);

    std::vector<Service> v{};
    v.reserve(native->services.size());

    for (const auto& s : native->services)
        v.emplace_back(s.second);

    return v;
}

std::optional<Service> Peripheral::service(const juce::Uuid& uuid) const
{
    jassert(native != nullptr);

    if (const auto it = native->services.find(uuid); it != native->services.cend())
        return it->second;

    return std::nullopt;
}

//===========================================================================================================================
struct Adapter::Native : private juce::Thread
{
    explicit Native(Adapter::Listener& l);

    ~Native() override;

    GDBusProxy    * proxy    = nullptr;
    GDBusClient   * client   = nullptr;
    GMainLoop     * mainLoop = nullptr;
    DBusConnection* dbus     = nullptr;

    Adapter::Listener& listener;

    std::vector<Peripheral> peripherals;

private:
    void run() override { g_main_loop_run(mainLoop); }
};

//===========================================================================================================================
Adapter::Adapter(Adapter::Listener& l) : native(std::make_unique<Adapter::Native>(l)) {}

Adapter::~Adapter() = default;

std::vector<Peripheral> Adapter::getConnectedPeripherals(const juce::Uuid&) const
{
    jassert(native != nullptr);

    std::vector<Peripheral> v{};
    for (const auto& p : native->peripherals)
        if (dbus::getProperty<bool>(p.native->proxy, "Connected"))
            v.emplace_back(p);

    return v;
}

void Adapter::startScan(const std::vector<juce::Uuid>& uuids)
{
    jassert(native != nullptr);

    constexpr int distance_invalid = 0x7fff;

    struct DiscoveryFilterArgs
    {
        const char* transport;
        uint16_t rssi;
        int16_t  pathloss;
        const std::vector<juce::Uuid>& uuids;
    };

    const auto setup = [](DBusMessageIter* iter, void* user_data)
    {
        const auto* args = static_cast<const DiscoveryFilterArgs*>(user_data);
        DBusMessageIter dict{};

        dbus_message_iter_open_container(iter, DBUS_TYPE_ARRAY,
                DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING
                DBUS_TYPE_STRING_AS_STRING
                DBUS_TYPE_VARIANT_AS_STRING
                DBUS_DICT_ENTRY_END_CHAR_AS_STRING, &dict);

        if (!args->uuids.empty())
        {
            DBusMessageIter entry, value, arrayIter;
            const char* uuids = "UUIDs";

            dbus_message_iter_open_container(&dict, DBUS_TYPE_DICT_ENTRY, nullptr, &entry);

            /* dict key */
            dbus_message_iter_append_basic(&entry, DBUS_TYPE_STRING, &uuids);
            dbus_message_iter_open_container(&entry, DBUS_TYPE_VARIANT, "as", &value);
            dbus_message_iter_open_container(&value, DBUS_TYPE_ARRAY, "s", &arrayIter);

            for (const auto& uuid: args->uuids)
            {
                const juce::String uuid_str = uuid.toDashedString();
                const char* str = uuid_str.getCharPointer();

                dbus_message_iter_append_basic(&arrayIter, DBUS_TYPE_STRING, &str);
            }

            dbus_message_iter_close_container(&value, &arrayIter);

            /* close vararg*/
            dbus_message_iter_close_container(&entry, &value);

            /* close entry */
            dbus_message_iter_close_container(&dict, &entry);
        }

        if (args->pathloss != distance_invalid)
            dict_append_entry(&dict, "Pathloss", DBUS_TYPE_UINT16, (void*) &args->pathloss);

        if (args->rssi != distance_invalid)
            dict_append_entry(&dict, "RSSI", DBUS_TYPE_INT16, (void*) &args->rssi);

        if (args->transport)
            dict_append_entry(&dict, "Transport", DBUS_TYPE_STRING, (void*) &args->transport);

        dbus_message_iter_close_container(iter, &dict);
    };

    const auto reply = [](DBusMessage* message, void*)
    {
        if (const auto err = dbus::getError(message); err.has_value())
                DBG("Failed to set discovery filter: " << err->get().name);
    };

    if (state() != State::Idle)
    {
        DBG("Need to be in idle state to start scan!");
        return;
    }

    if (!uuids.empty())
    {
        const DiscoveryFilterArgs args{"le", distance_invalid, distance_invalid, uuids};

        g_dbus_proxy_method_call(native->proxy, "SetDiscoveryFilter", setup, reply, (void*) &args, nullptr);
    }

    const auto discovery_started = [](DBusMessage* message, void*)
    {
        if (const auto err = dbus::getError(message); err.has_value())
                DBG("Failed to start discovery: " << err->get().name);
    };

    g_dbus_proxy_method_call(native->proxy, "StartDiscovery", nullptr, discovery_started, nullptr, nullptr);
}

void Adapter::stopScan()
{
    jassert(native != nullptr);

    if (state() != State::Discovering)
        return;

    const auto reply = [](DBusMessage* message, void*)
    {
        if (const auto err = dbus::getError(message); err.has_value())
                DBG("Failed to stop discovery: " << err->get().name);
    };

    g_dbus_proxy_method_call(native->proxy, "StopDiscovery", nullptr, reply, nullptr, nullptr);
}

void Adapter::connect(const Peripheral& peripheral)
{
    jassert(peripheral.native != nullptr);

    if (dbus::getProperty<bool>(peripheral.native->proxy, "Connected"))
    {
        native->listener.peripheralStateUpdated(peripheral);
        return;
    }

    const auto reply = [](DBusMessage* message, void*)
    {
        if (const auto err = dbus::getError(message); err.has_value())
                DBG("Failed to connect: " << err->get().name);
    };

    if (!g_dbus_proxy_method_call(peripheral.native->proxy, "Connect", nullptr, reply, nullptr, nullptr))
            DBG("D-Bus method call failed: Connect");
}

Adapter::State Adapter::state() const
{
    jassert(native != nullptr);

    // TODO: There are more properties relating to the state of the adapter:
    //       Discoverable, Discovering, Pairable, Powered
    return dbus::getProperty<bool>(native->proxy, "Powered") ? State::Idle : State::PoweredOff;
}

Adapter::Native::Native(Adapter::Listener& l)
        : juce::Thread("BlueZ"),
          listener(l)
{
    mainLoop = g_main_loop_new(nullptr, false);
    dbus     = g_dbus_setup_bus(DBUS_BUS_SYSTEM, nullptr, nullptr);
    client   = g_dbus_client_new(dbus, "org.bluez", "/org/bluez");

    const auto dbus_connect    = [](DBusConnection*, void*) { DBG("DBus connected"); };
    const auto dbus_disconnect = [](DBusConnection*, void*) { DBG("DBus disconnected"); };
    const auto dbus_signal     = [](DBusConnection*, DBusMessage* msg, void*)
    {
        printf("D-Bus message: %s.%s\n", dbus_message_get_interface(msg), dbus_message_get_member(msg));

    };

    g_dbus_client_set_connect_watch(client, dbus_connect, nullptr);
    g_dbus_client_set_disconnect_watch(client, dbus_disconnect, nullptr);
    g_dbus_client_set_signal_watch(client, dbus_signal, nullptr);

    const auto dbus_proxy_added = [](GDBusProxy* proxy, void* user_data)
    {
        jassert(user_data != nullptr);

        const juce::String interface(g_dbus_proxy_get_interface(proxy));

        DBG("D-Bbus proxy added: " << interface);

        auto* adapter = static_cast<Native*>(user_data);

        if (interface == "org.bluez.Adapter1")
        {
            adapter->proxy = proxy;

//            adapter->listener.stateUpdated();
        }
        else if (interface == "org.bluez.Device1")
        {
            const auto& p = adapter->peripherals.emplace_back(proxy);

            adapter->listener.peripheralDiscovered(p);
        }
        else if (interface == "org.bluez.GattService1")
        {
            const auto* device_path = dbus::getProperty<const char*>(proxy, "Device");

            const auto it = std::find_if(adapter->peripherals.begin(), adapter->peripherals.end(), [&](const auto& p)
            {
                return dbus::getObjectPath(p.native->proxy) == std::string_view(device_path);
            });

            if (it != adapter->peripherals.end())
            {
                const Service service(Service::Native(proxy, it->native));
                it->native->services.try_emplace(service.uuid(), service);
            }
        }
        else if (interface == "org.bluez.GattCharacteristic1")
        {
            const auto* service_path = dbus::getProperty<const char*>(proxy, "Service");

            for (const auto& p : adapter->peripherals)
            {
                const auto it = std::find_if(p.native->services.cbegin(), p.native->services.cend(), [&](const auto& s)
                {
                    return dbus::getObjectPath(s.second.native->proxy) == std::string_view(service_path);
                });

                if (it != p.native->services.cend())
                {
                    const Characteristic charact(proxy);
                    it->second.native->characteristics.try_emplace(charact.uuid(), charact);
                }
            }
        }
        else if (interface == "org.bluez.GattDescriptor1")
        {}
    };

    const auto dbus_proxy_removed = [](GDBusProxy* proxy, void* user_data)
    {
        jassert(user_data != nullptr);

        const char* interface = g_dbus_proxy_get_interface(proxy);
        DBG("D-Bus proxy removed: " << juce::String(interface));

        // TODO: Any actions that need to be taken care of?
    };

    const auto dbus_property_changed = [](GDBusProxy* proxy, const char* prop, DBusMessageIter* it, void* user_data)
    {
        jassert(user_data != nullptr);

        const juce::String interface(g_dbus_proxy_get_interface(proxy));

        DBG("D-Bus property changed: " << interface << ", " << prop);

        auto* adapter = static_cast<Native*>(user_data);

        if (interface == "org.bluez.Adapter1")
        {
            // TODO: There are more properties related to the state of the adapter
            if (strcmp(prop, "Discovering") == 0)
            {
                dbus_bool_t discovering{};
                dbus_message_iter_get_basic(it, &discovering);

                adapter->listener.stateUpdated();
            }
        }

        if (interface == "org.bluez.Device1")
        {
            if (strcmp(prop, "Connected") == 0 || strcmp(prop, "ServicesResolved") == 0)
            {
                dbus_bool_t connected{};
                dbus_message_iter_get_basic(it, &connected);

                const auto it = std::find_if(adapter->peripherals.cbegin(), adapter->peripherals.cend(), [&](const auto& p)
                {
                    return dbus::getObjectPath(p.native->proxy) == dbus::getObjectPath(proxy);
                });

                if (it != adapter->peripherals.cend())
                    adapter->listener.peripheralStateUpdated(*it);
            }

            if (strcmp(prop, "RSSI") == 0)
            {
                // TODO: Invalid RSSI?
                dbus_int16_t rssi{};
                dbus_message_iter_get_basic(it, &rssi);

                adapter->listener.peripheralRssiUpdated({proxy}, (double) rssi);
            }
        }

        if (interface == "org.bluez.GattCharacteristic1")
        {
            if (strcmp(prop, "Value") == 0)
            {
                if (dbus_message_iter_get_arg_type(it) == DBUS_TYPE_ARRAY)
                {
                    DBusMessageIter array{};
                    dbus_message_iter_recurse(it, &array);

                    const uint8_t* value{};
                    int len{};
                    dbus_message_iter_get_fixed_array(&array, &value, &len);

                    const auto bytes = gsl::as_bytes(gsl::make_span(value, len));

                    dbus::Proxy service(adapter->client, dbus::getProperty<const char*>(proxy, "Service"), "org.bluez.GattService1");
                    dbus::Proxy device(adapter->client, dbus::getProperty<const char*>(service.get(), "Device"), "org.bluez.Device1");

                    const auto it = std::find_if(adapter->peripherals.cbegin(), adapter->peripherals.cend(), [&](const auto& p)
                    {
                        return p.native->proxy == device.get();
                    });

                    if (it != adapter->peripherals.cend())
                        it->native->listener->onValueChanged({proxy}, gsl::as_bytes(gsl::make_span(value, len)));
                }
            }
            else if (strcmp(prop, "Notifying") == 0)
            {
                dbus::Proxy service(adapter->client, dbus::getProperty<const char*>(proxy, "Service"), "org.bluez.GattService1");
                dbus::Proxy device(adapter->client, dbus::getProperty<const char*>(service.get(), "Device"), "org.bluez.Device1");

                const auto it = std::find_if(adapter->peripherals.cbegin(), adapter->peripherals.cend(), [&](const auto& p)
                {
                    return p.native->proxy == device.get();
                });

                if (it != adapter->peripherals.cend())
                {
                    const Service srv(Service::Native(service.get(), it->native));

                    const auto& s = it->native->services;
                    if (const auto srv_it = s.find(srv.uuid()); srv_it != s.cend())
                    {
                        if (auto p = srv_it->second.native->peripheral.lock())
                            if (p->listener != nullptr)
                                p->listener->onNotificationStateUpdated({proxy});
                    }
                }
            }
        }
    };

    const auto dbus_client_ready = [](GDBusClient* client, void* user_data)
    {
        DBG("D-Bus client ready");

        auto* adapter = static_cast<Native*>(user_data);

        if (client == adapter->client)
            adapter->listener.stateUpdated();
    };

    g_dbus_client_set_proxy_handlers(client, dbus_proxy_added, dbus_proxy_removed, dbus_property_changed, this);
    g_dbus_client_set_ready_watch(client, dbus_client_ready, this);

    startThread();
}

Adapter::Native::~Native()
{
    g_main_loop_quit(mainLoop);

    g_dbus_client_unref(client);
    dbus_connection_unref(dbus);
    g_main_loop_unref(mainLoop);

    stopThread(1000);
}

} // namespace ble
