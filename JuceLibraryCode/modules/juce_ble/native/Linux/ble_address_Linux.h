#ifndef BLE_ADDRESS_LINUX_H
#define BLE_ADDRESS_LINUX_H

#include <juce_core/juce_core.h>

namespace ble {
class Address {
public:
    Address() = default;

    Address(const juce::String& str) : address(str) {}
    Address(const char bytes[6]) : address(bytes) {}

    [[nodiscard]] juce::String toString() const {return address.toString(); }

    bool operator==(const Address& other) const { return address == other.address; }

private:
    const juce::MACAddress address;
};
}

#endif  // BLE_ADDRESS_LINUX_H