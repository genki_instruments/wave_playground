# Wave Playground

Wave Playground is a cross-platform C++ application for showcasing the Wave dev-kit and its minimal API.

For more info please see [the documentation](https://www.notion.so/genkiinstruments/Wave-API-8a91bd3553ee4529878342dec477d93f).
