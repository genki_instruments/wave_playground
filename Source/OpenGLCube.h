/*
  ==============================================================================

    OpenGLCube.h
    Created: 2 Oct 2018 9:05:47am
    Author:  dingari

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
class OpenGLCube   :    public OpenGLAppComponent,
                        public Slider::Listener
{
public:
    //==============================================================================
    OpenGLCube() :
            angleSlider(Slider::LinearHorizontal, Slider::NoTextBox),
            xSlider(Slider::LinearHorizontal, Slider::NoTextBox),
            ySlider(Slider::LinearHorizontal, Slider::NoTextBox),
            zSlider(Slider::LinearHorizontal, Slider::NoTextBox)
    {
//        addAndMakeVisible(angleSlider);
//        addAndMakeVisible(xSlider);
//        addAndMakeVisible(ySlider);
//        addAndMakeVisible(zSlider);
//
//        angleSlider.setRange(Range<double>(-MathConstants<double>::pi, MathConstants<double>::pi), dontSendNotification);
//        xSlider.setRange(Range<double>(-1, 1), dontSendNotification);
//        ySlider.setRange(Range<double>(-1, 1), dontSendNotification);
//        zSlider.setRange(Range<double>(-1, 1), dontSendNotification);
//
//        angleSlider.setPopupDisplayEnabled(true, true, this);
//        xSlider.setPopupDisplayEnabled(true, true, this);
//        ySlider.setPopupDisplayEnabled(true, true, this);
//        zSlider.setPopupDisplayEnabled(true, true, this);
//
//        xSlider.setValue(1, dontSendNotification);
//
//        angleSlider.addListener(this);
//        xSlider.addListener(this);
//        ySlider.addListener(this);
//        zSlider.addListener(this);

        setSize(800, 600);
    }

    ~OpenGLCube()
    {
        shutdownOpenGL();
    }

    void initialise() override
    {
        createShaders();
    }

    void shutdown() override
    {
        shader.reset();
        cube.reset();
        coneX.reset();
        coneY.reset();
        coneZ.reset();
        attributes.reset();
        uniforms.reset();
    }

    void sliderValueChanged(Slider* slider) override
    {
        // DBG
        const float angle = angleSlider.getValue();
        const float x = xSlider.getValue();
        const float y = ySlider.getValue();
        const float z = zSlider.getValue();

        qTransformWaveToOpenGL = Quaternion<float>::fromAngle(-angle, {x, y, z});
    }

    Matrix3D<float> getProjectionMatrix() const
    {
        float w = 1.0f / (0.5f + 0.1f);
        float h = w * getLocalBounds().toFloat().getAspectRatio (false);
        return Matrix3D<float>::fromFrustum (-w, w, -h, h, 4.0f, 30.0f);
    }

    Matrix3D<float> getViewMatrix() const
    {
        const Matrix3D<float> viewMatrix (Vector3D<float> (0.0f, 0.0f, -10.0f));
        const Matrix3D<float> rotationMatrix = pose.getRotationMatrix();

        return rotationMatrix * viewMatrix;
    }

    void render() override
    {
        jassert (OpenGLHelpers::isContextActive());

        auto desktopScale = (float) openGLContext.getRenderingScale();
        OpenGLHelpers::clear(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glViewport(0, 0, roundToInt(desktopScale * getWidth()), roundToInt(desktopScale * getHeight()));

        shader->use();

        if (uniforms->projectionMatrix.get() != nullptr)
            uniforms->projectionMatrix->setMatrix4(getProjectionMatrix().mat, 1, false);

        if (uniforms->viewMatrix.get() != nullptr)
            uniforms->viewMatrix->setMatrix4(getViewMatrix().mat, 1, false);

        cube->draw(openGLContext, *attributes);
        coneX->draw(openGLContext, *attributes);
        coneY->draw(openGLContext, *attributes);
        coneZ->draw(openGLContext, *attributes);

        // Reset the element buffers so child Components draw correctly
        openGLContext.extensions.glBindBuffer(GL_ARRAY_BUFFER, 0);
        openGLContext.extensions.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    }

    void paint(Graphics& g) override
    {

    }

    void resized() override
    {
        // This is called when this component is resized.
        // If you add any child components, this is where you should
        // update their positions.

        auto sliderBounds = getLocalBounds().removeFromBottom(30);
        auto w = sliderBounds.proportionOfWidth(1 / 4.0);

        angleSlider.setBounds(sliderBounds.removeFromLeft(w).reduced(5));
        xSlider.setBounds(sliderBounds.removeFromLeft(w).reduced(5));
        ySlider.setBounds(sliderBounds.removeFromLeft(w).reduced(5));
        zSlider.setBounds(sliderBounds.removeFromLeft(w).reduced(5));

    }

    void createShaders()
    {
        vertexShader =
                "attribute vec4 position;\n"
                "attribute vec4 sourceColour;\n"
                "attribute vec2 textureCoordIn;\n"
                "\n"
                "uniform mat4 projectionMatrix;\n"
                "uniform mat4 viewMatrix;\n"
                "\n"
                "varying vec4 destinationColour;\n"
                "varying vec2 textureCoordOut;\n"
                "\n"
                "void main()\n"
                "{\n"
                "    destinationColour = sourceColour;\n"
                "    textureCoordOut = textureCoordIn;\n"
                "    gl_Position = projectionMatrix * viewMatrix * position;\n"
                "}\n";

        fragmentShader =
#if JUCE_OPENGL_ES
        "varying lowp vec4 destinationColour;\n"
            "varying lowp vec2 textureCoordOut;\n"
#else
                "varying vec4 destinationColour;\n"
                "varying vec2 textureCoordOut;\n"
                #endif
                "\n"
                "void main()\n"
                "{\n"
                "    gl_FragColor = destinationColour;\n"
                "}\n";

        std::unique_ptr<OpenGLShaderProgram> newShader(new OpenGLShaderProgram(openGLContext));
        String statusText;

        if (newShader->addVertexShader(OpenGLHelpers::translateVertexShaderToV3(vertexShader))
            && newShader->addFragmentShader(OpenGLHelpers::translateFragmentShaderToV3(fragmentShader))
            && newShader->link()) {
            cube.reset();
            coneX.reset();
            coneY.reset();
            coneZ.reset();
            attributes.reset();
            uniforms.reset();

            shader.reset(newShader.release());
            shader->use();

            cube.reset (new Cube (openGLContext));
            coneX.reset (new Cone (openGLContext, Cone::Axis::X, juce::Colours::red));
            coneY.reset (new Cone (openGLContext, Cone::Axis::Y, juce::Colour(0xff00ff00)));
            coneZ.reset (new Cone (openGLContext, Cone::Axis::Z, juce::Colours::blue));
            attributes.reset(new Attributes(openGLContext, *shader));
            uniforms.reset(new Uniforms(openGLContext, *shader));

            statusText = "GLSL: v" + String(OpenGLShaderProgram::getLanguageVersion(), 2);
        } else {
            statusText = newShader->getLastError();
        }
    }

    void setPose(const juce::Quaternion<float>& q) { pose = q; }

private:
    //==============================================================================
    struct Vertex
    {
        float position[3];
        float normal[3];
        float colour[4];
        float texCoord[2];
    };

    //==============================================================================
    // This class just manages the attributes that the shaders use.
    struct Attributes
    {
        Attributes(OpenGLContext& openGLContext, OpenGLShaderProgram& shaderProgram)
        {
            position.reset(createAttribute(openGLContext, shaderProgram, "position"));
            normal.reset(createAttribute(openGLContext, shaderProgram, "normal"));
            sourceColour.reset(createAttribute(openGLContext, shaderProgram, "sourceColour"));
            textureCoordIn.reset(createAttribute(openGLContext, shaderProgram, "textureCoordIn"));
        }

        void enable(OpenGLContext& glContext)
        {
            if (position.get() != nullptr) {
                glContext.extensions.glVertexAttribPointer(position->attributeID, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
                glContext.extensions.glEnableVertexAttribArray(position->attributeID);
            }

            if (normal.get() != nullptr) {
                glContext.extensions.glVertexAttribPointer(normal->attributeID, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) * 3));
                glContext.extensions.glEnableVertexAttribArray(normal->attributeID);
            }

            if (sourceColour.get() != nullptr) {
                glContext.extensions.glVertexAttribPointer(sourceColour->attributeID, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) * 6));
                glContext.extensions.glEnableVertexAttribArray(sourceColour->attributeID);
            }

            if (textureCoordIn.get() != nullptr) {
                glContext.extensions.glVertexAttribPointer(textureCoordIn->attributeID, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) * 10));
                glContext.extensions.glEnableVertexAttribArray(textureCoordIn->attributeID);
            }
        }

        void disable(OpenGLContext& glContext)
        {
            if (position.get() != nullptr) glContext.extensions.glDisableVertexAttribArray(position->attributeID);
            if (normal.get() != nullptr) glContext.extensions.glDisableVertexAttribArray(normal->attributeID);
            if (sourceColour.get() != nullptr) glContext.extensions.glDisableVertexAttribArray(sourceColour->attributeID);
            if (textureCoordIn.get() != nullptr) glContext.extensions.glDisableVertexAttribArray(textureCoordIn->attributeID);
        }

        std::unique_ptr<OpenGLShaderProgram::Attribute> position, normal, sourceColour, textureCoordIn;

    private:
        static OpenGLShaderProgram::Attribute* createAttribute(OpenGLContext& openGLContext,
                                                               OpenGLShaderProgram& shader,
                                                               const char* attributeName)
        {
            if (openGLContext.extensions.glGetAttribLocation(shader.getProgramID(), attributeName) < 0)
                return nullptr;

            return new OpenGLShaderProgram::Attribute(shader, attributeName);
        }
    };

    //==============================================================================
    // This class just manages the uniform values that the demo shaders use.
    struct Uniforms
    {
        Uniforms(OpenGLContext& openGLContext, OpenGLShaderProgram& shaderProgram)
        {
            projectionMatrix.reset(createUniform(openGLContext, shaderProgram, "projectionMatrix"));
            viewMatrix.reset(createUniform(openGLContext, shaderProgram, "viewMatrix"));
        }

        std::unique_ptr<OpenGLShaderProgram::Uniform> projectionMatrix, viewMatrix;

    private:
        static OpenGLShaderProgram::Uniform* createUniform(OpenGLContext& openGLContext,
                                                           OpenGLShaderProgram& shaderProgram,
                                                           const char* uniformName)
        {
            if (openGLContext.extensions.glGetUniformLocation(shaderProgram.getProgramID(), uniformName) < 0)
                return nullptr;

            return new OpenGLShaderProgram::Uniform(shaderProgram, uniformName);
        }
    };

    //==============================================================================
    struct Cube
    {
        Cube (OpenGLContext& glContext)
        {
            vertexBuffers.add(new VertexBuffer(glContext));
        }

        void draw (OpenGLContext& glContext, Attributes& glAttributes)
        {
            for (auto* vertexBuffer : vertexBuffers)
            {
                vertexBuffer->bind();

                glAttributes.enable (glContext);

                glEnable(GL_DEPTH_TEST);
                glDepthFunc(GL_LESS);

                glDrawArrays(GL_TRIANGLES, 0, vertexBuffer->numVertices);
                glAttributes.disable (glContext);
            }
        }

    private:
        struct VertexBuffer
        {
            VertexBuffer (OpenGLContext& context) : openGLContext (context)
            {
                // Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
                // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
                numVertices = 6 * (3 * 2);

                openGLContext.extensions.glGenBuffers (1, &vertexBuffer);
                openGLContext.extensions.glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);

                Array<Vertex> vertices;

                // Front face
                auto color = juce::Colours::blue;
                vertices.add({{-1.0f, -1.0f, 1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});      // 0
                vertices.add({{-1.0f,  1.0f, 1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});      // 1
                vertices.add({{ 1.0f,  1.0f, 1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});      // 2
                vertices.add({{-1.0f, -1.0f, 1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});      // 0
                vertices.add({{ 1.0f,  1.0f, 1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});      // 2
                vertices.add({{ 1.0f, -1.0f, 1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});      // 3

                // Back face
                color = juce::Colours::blue;
                vertices.add({{-1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 4
                vertices.add({{-1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 5
                vertices.add({{ 1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 6
                vertices.add({{-1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 4
                vertices.add({{ 1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 6
                vertices.add({{ 1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 7

                // Left face
                color = juce::Colours::red;
                vertices.add({{-1.0f, -1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 0
                vertices.add({{-1.0f,  1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 1
                vertices.add({{-1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 4
                vertices.add({{-1.0f,  1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 1
                vertices.add({{-1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 4
                vertices.add({{-1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 5

                // Right face
                color = juce::Colours::red;
                vertices.add({{ 1.0f,  1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 2
                vertices.add({{ 1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 6
                vertices.add({{ 1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 7
                vertices.add({{ 1.0f,  1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 2
                vertices.add({{ 1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 7
                vertices.add({{ 1.0f, -1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 3

                // Top face
                color = juce::Colour(0xff00ff00);
                vertices.add({{-1.0f,  1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 1
                vertices.add({{ 1.0f,  1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 2
                vertices.add({{-1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 5
                vertices.add({{ 1.0f,  1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 2
                vertices.add({{-1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 5
                vertices.add({{ 1.0f,  1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 6

                // Bottom face
                color = juce::Colour(0xff00ff00);
                vertices.add({{ 1.0f, -1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 3
                vertices.add({{-1.0f, -1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 0
                vertices.add({{-1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 4
                vertices.add({{ 1.0f, -1.0f,  1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 3
                vertices.add({{-1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 4
                vertices.add({{ 1.0f, -1.0f, -1.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});     // 7

                const auto scale = 0.75;
                for (auto& v : vertices) {
                    v.position[0] *= scale;
                    v.position[1] *= scale;
                    v.position[2] *= scale;
                }

                openGLContext.extensions.glBufferData (GL_ARRAY_BUFFER,
                        static_cast<GLsizeiptr> (static_cast<size_t> (vertices.size()) * sizeof (Vertex)),
                        vertices.getRawDataPointer(), GL_STATIC_DRAW);

            }

            ~VertexBuffer()
            {
                openGLContext.extensions.glDeleteBuffers (1, &vertexBuffer);
                openGLContext.extensions.glDeleteBuffers (1, &indexBuffer);
            }

            void bind()
            {
                openGLContext.extensions.glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
                openGLContext.extensions.glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
            }

            GLuint vertexBuffer, indexBuffer;
            int numIndices, numVertices;

            OpenGLContext& openGLContext;

            JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (VertexBuffer)
        };

        OwnedArray<VertexBuffer> vertexBuffers;
    };

    //==============================================================================
    struct Cone
    {
        enum Axis { X, Y, Z };

        Cone (OpenGLContext& glContext, Axis axis, const juce::Colour& color)
        {
            vertexBuffers.add(new VertexBuffer(glContext, axis, color));
        }

        void draw (OpenGLContext& glContext, Attributes& glAttributes)
        {
            for (auto* vertexBuffer : vertexBuffers)
            {
                vertexBuffer->bind();

                glAttributes.enable (glContext);

                glEnable(GL_DEPTH_TEST);
                glDepthFunc(GL_LESS);

                glDrawArrays(GL_TRIANGLE_FAN, 0, vertexBuffer->numVertices);
                glAttributes.disable (glContext);
            }
        }

    private:
        struct VertexBuffer
        {
            VertexBuffer (OpenGLContext& context, Cone::Axis axis, const juce::Colour c) :
                openGLContext (context),
                color(c)
            {
                numVertices = 6;

                openGLContext.extensions.glGenBuffers (1, &vertexBuffer);
                openGLContext.extensions.glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);

                Array<Vertex> vertices;

                vertices.add({{0.0f, 0.0f, 0.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});

                switch (axis) {
                    case X:
                        vertices.add({{4.0f,  0.0f, -0.5f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{4.0f,  0.5f,  0.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{4.0f,  0.0f,  0.5f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{4.0f, -0.5f,  0.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{4.0f,  0.0f, -0.5f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        break;

                    case Y:
                        vertices.add({{ 0.0f, 4.0f, -0.5f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{ 0.5f, 4.0f,  0.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{ 0.0f, 4.0f,  0.5f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{-0.5f, 4.0f,  0.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{ 0.0f, 4.0f, -0.5f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        break;

                    case Z:
                        vertices.add({{ 0.0f, -0.5f, 4.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{ 0.5f,  0.0f, 4.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{ 0.0f,  0.5f, 4.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{-0.5f,  0.0f, 4.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        vertices.add({{ 0.0f, -0.5f, 4.0f}, { 0.5f, 0.5f, 0.5f }, {color.getFloatRed(), color.getFloatGreen(), color.getFloatBlue(), color.getFloatAlpha()}, { 0.5f, 0.5f }});
                        break;

                    default:
                        break;
                }

                const auto scale = 0.75;
                for (auto& v : vertices) {
                    v.position[0] *= scale;
                    v.position[1] *= scale;
                    v.position[2] *= scale;
                }

                numVertices = vertices.size();

                openGLContext.extensions.glBufferData (GL_ARRAY_BUFFER,
                        static_cast<GLsizeiptr> (static_cast<size_t> (vertices.size()) * sizeof (Vertex)),
                        vertices.getRawDataPointer(), GL_STATIC_DRAW);

            }

            ~VertexBuffer()
            {
                openGLContext.extensions.glDeleteBuffers (1, &vertexBuffer);
                openGLContext.extensions.glDeleteBuffers (1, &indexBuffer);
            }

            void bind()
            {
                openGLContext.extensions.glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
                openGLContext.extensions.glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
            }

            GLuint vertexBuffer, indexBuffer;
            int numIndices, numVertices;

            OpenGLContext& openGLContext;

            juce::Colour color;

            JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (VertexBuffer)
        };

        OwnedArray<VertexBuffer> vertexBuffers;
    };

    const char* vertexShader;
    const char* fragmentShader;

    std::unique_ptr<OpenGLShaderProgram> shader;
    std::unique_ptr<Cube> cube;
    std::unique_ptr<Cone> coneX, coneY, coneZ;
    std::unique_ptr<Attributes> attributes;
    std::unique_ptr<Uniforms> uniforms;

    String newVertexShader, newFragmentShader;

    Quaternion<float> pose{0, 0, 0, 1}, qTransformWaveToOpenGL{0, 0, 0, 1};

    Slider angleSlider, xSlider, ySlider, zSlider;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OpenGLCube);
};