#include "PlaygroundComponent.h"

#include "drow_Utilities.h"

constexpr std::array<std::array<uint8_t, FrameComponent::N>, FrameComponent::M> DefaultApiFrame{
        {
                {000, 255, 000, 000, 255, 255, 000, 000, 255},
                {255, 000, 255, 000, 255, 000, 255, 000, 255},
                {255, 255, 255, 000, 255, 255, 000, 000, 255},
                {255, 000, 255, 000, 255, 000, 000, 000, 255},
                {000, 000, 000, 000, 000, 000, 000, 000, 000},
        }
};

PlaygroundComponent::PlaygroundComponent() :
        ble(*this),
        sm(static_cast<MessageListener&>(*this), ble, peripheral, logger),
        accelGraph(3, -1.0f, 2.0f, "Accelerometer"),
        gyroGraph(3, -1000.0f, 1000.0f, "Gyroscope"),
        eulerGraph(3, -MathConstants<float>::pi, MathConstants<float>::pi, "Euler angles")
{
    drow::addAndMakeVisible(*this, {&gyroGraph, &accelGraph, &eulerGraph, &sampleRateLabel, &batteryLabel, &handwritingDemo, &cube, &googleResult, &googleStatus, &led});

    googleResult.setFont(Font(84.0f));
    googleResult.setJustificationType(Justification::centred);

    googleStatus.setFont(16.0f);

    handwritingDemo.setJustificationType(Justification::centred);

    led.onChange = [this] { sendRequest(Wave::Api::Query::Id::DisplayFrame, led.getFrame()); };

    led.setFrame(DefaultApiFrame);

    stateUpdated();

    startTimer(1000);

    setSize(800, 700);
}

PlaygroundComponent::~PlaygroundComponent()
{
    if (isConnected())
        sendRequest(Wave::Api::Query::Id::DeviceMode, Wave::DeviceMode::Preset);
}

void PlaygroundComponent::paint(Graphics& g)
{
    g.fillAll(Colour(0xff231f20));

    g.setColour(Colours::white);
    g.fillEllipse(display_pos.x, display_pos.y, 5, 5);

    for (const auto& p : paths)
        g.strokePath(p, PathStrokeType(1));
}

void PlaygroundComponent::resized()
{
    auto r = getLocalBounds();

    auto right = r.removeFromRight(r.proportionOfWidth(0.5)).reduced(5);
    r = r.reduced(5);

    //==================================================================================================================
    auto labels = r.removeFromBottom(30);
    sampleRateLabel.setBounds(labels.removeFromLeft(130));
    batteryLabel.setBounds(labels);

    led.setBounds(r.removeFromBottom(r.proportionOfHeight(0.4)));

    const auto height = r.proportionOfHeight(1 / 3.0);
    gyroGraph.setBounds(r.removeFromTop(height).reduced(5));
    accelGraph.setBounds(r.removeFromTop(height).reduced(5));
    eulerGraph.setBounds(r.removeFromTop(height).reduced(5));

    //==================================================================================================================
    cube.setBounds(right.removeFromTop(right.proportionOfHeight(0.5)));

    googleStatus.setBounds(right.removeFromBottom(30));
    handwritingDemo.setBounds(right.removeFromTop(30));
    drawing_board = right;
    googleResult.setBounds(right);
}

void PlaygroundComponent::handleMessage(const Message& msg)
{
    if (dynamic_cast<const SendFrame*>(&msg))
    {
        sendRequest(Wave::Api::Query::Id::DisplayFrame, led.getFrame());
    }
    else if (const auto* g = dynamic_cast<const UpdateGoogleResult*>(&msg))
    {
        paths.clear();
        googleResult.setText(g->text, dontSendNotification);
    }
    else if (const auto* g = dynamic_cast<const UpdateGoogleStatus*>(&msg))
    {
        googleStatus.setText(g->text, dontSendNotification);
    }
}

void PlaygroundComponent::newData(const Wave::Datastream::SensorData& d, const Wave::Datastream::MotionData& m)
{
    //==================================================================================================================
    const auto  p   = MathConstants<float>::pi / 8.0f;
    const float w   = drawing_board.getWidth();
    const float h   = drawing_board.getHeight();
    const float lim = std::tan(p);
    display_pos = {
            drawing_board.getX() + jmap((float) std::tan(jlimit(-p, p, m.euler[2])), -lim, lim, 0.0f, w),
            drawing_board.getY() + jmap((float) std::tan(jlimit(-p, p, -m.euler[1])), -lim, lim, 0.0f, h)
    };

    if (is_drawing)
    {
        auto& path = paths.back();
        if (path.isEmpty())
        {
            path.startNewSubPath(display_pos);
            path.lineTo(display_pos);
        }
        else
        {
            path.lineTo(display_pos);
        }
    }

    //==================================================================================================================
    gyroGraph.append(std::vector<double>(d.gyro.cbegin(), d.gyro.cend()));
    accelGraph.append(std::vector<double>(d.accel.cbegin(), d.accel.cend()));
    eulerGraph.append(std::vector<double>(m.euler.cbegin(), m.euler.cend()));

    //==================================================================================================================
    auto openGlPose = Quaternion<float>(-m.currentPose[1], -m.currentPose[2], -m.currentPose[3], m.currentPose[0]);

    // Rotating in OpenGL coordinate system
    openGlPose *= Quaternion<float>::fromAngle(-MathConstants<float>::halfPi, {0.0f, 1.0f, 0.0f});
    openGlPose *= Quaternion<float>::fromAngle(-MathConstants<float>::halfPi, {0.0f, 0.0f, 1.0f});

    cube.setPose(openGlPose);

    //==================================================================================================================
    sampleRateLabel.setText(String("Sample rate: ") + String((int) std::nearbyint(1 / (d.timestamp - prevTimestamp))) + String(" Hz"), dontSendNotification);
    prevTimestamp = d.timestamp;

    //==================================================================================================================
    repaint();
}

void PlaygroundComponent::handleButtonEvent(const Wave::ButtonEvent& e)
{
    using namespace Wave;
    std::printf("Button event received: %hhu %hhu\n", e.id, e.action);

    switch (e.id)
    {
        case ButtonId::A:
            if (e.action == ButtonAction::Click)
            {
                paths.clear();
                googleResult.setText("", dontSendNotification);
                sendRequest(Wave::Api::Query::Id::Recenter);
            }

            break;

        case ButtonId::B:
            if (e.action == ButtonAction::Down && !is_drawing)
            {
                paths.emplace_back();
                is_drawing = true;
            }
            else if (is_drawing && (e.action == ButtonAction::Up || e.action == ButtonAction::LongUp || e.action == ButtonAction::ExtraLongUp))
            {
                is_drawing = false;
            }

            break;

        case ButtonId::C:
            if (e.action == ButtonAction::Click)
            {
                processPaths();
            }

            break;
        case ButtonId::D:
            break;
    }
}

void PlaygroundComponent::processPaths()
{
    //==================================================================================================================
    const auto parse_paths = [](const auto& paths)
    {
        var root;

        const auto call_n_times = [](int n, auto& it)
        {
            for (int i = 0; i < n; ++i)
                if (!it.next())
                    return false;

            return true;
        };

        for (const auto& p : paths)
        {
            Path::Iterator it(p);

            var xs, ys;

            while (call_n_times(10, it))
            {
                xs.append((int) it.x1);
                ys.append((int) it.y1);
            }

            root.append(Array<var>{xs, ys});
        }

        return root;
    };

    //==================================================================================================================
    if (paths.empty())
        return;

    request.header("Content-Type", "application/json");

    auto* req           = new DynamicObject();
    auto* writing_guide = new DynamicObject();

    writing_guide->setProperty("writing_area_width", drawing_board.getWidth());
    writing_guide->setProperty("writing_area_height", drawing_board.getHeight());

    const auto ink = parse_paths(paths);
    req->setProperty("writing_guide", writing_guide);
    req->setProperty("ink", ink);
    req->setProperty("language", "en");

    const auto v = var(req);
    std::printf("%s\n", JSON::toString(v).toStdString().c_str());

    googleStatus.setText("Sending request", dontSendNotification);

    //==================================================================================================================
    juce::Thread::launch([v, r = &request, weak_ref = WeakReference<MessageListener>(this)]
    {
        const auto response = r->post("inputtools/request?ime=handwriting&app=mobilesearch&cs=1&oe=UTF-8")
                .field("options", "enable_pre_space")
                .field("requests", Array<var>{v})
                .execute();

        if (response.result.failed())
        {
            DBG(String("Error: %s, status: %d") + response.result.getErrorMessage() + String(response.status));

            if (auto* c = weak_ref.get())
                c->postMessage(new UpdateGoogleStatus("Response error"));
        }
        else
        {
            std::printf("Headers: %s\n", response.headers.getDescription().toStdString().c_str());
            std::printf("Body: %s\n", response.bodyAsString.toStdString().c_str());

            const auto& b = response.body;
            if (b[0] == "SUCCESS")
            {
                if (auto* c = weak_ref.get())
                {
                    if (b[1][0][1].size() > 0)
                    {
                        c->postMessage(new UpdateGoogleStatus("Success"));
                        c->postMessage(new UpdateGoogleResult(b[1][0][1][0].toString()));
                    }
                    else
                    {
                        c->postMessage(new UpdateGoogleStatus("Failed to parse"));
                    }
                }
            }
        }
    });
}
