/*
  ==============================================================================

    BluetoothFSM.h
    Created: 5 Sep 2019 9:04:20pm
    Author:  Kjartan Örn Bogason

  ==============================================================================
*/

#pragma once

#include "JuceHeader.h"
#include <optional>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wzero-as-null-pointer-constant"
#pragma clang diagnostic ignored "-Wshadow-field-in-constructor"
#pragma clang diagnostic ignored "-Wshadow-field"
#pragma clang diagnostic ignored "-Wsign-conversion"

#include <sml.hpp>

#pragma clang diagnostic pop

#else
#include <sml.hpp>

#endif

//======================================================================================================================
struct LoggerFSM_juce
{
    template<class SM, class TEvent>
    void log_process_event(const TEvent&)
    {
        DBG("[" << boost::sml::aux::get_type_name<SM>() << "][process_event] " << boost::sml::aux::get_type_name<TEvent>());
    }

    template<class SM, class TGuard, class TEvent>
    void log_guard(const TGuard&, const TEvent&, bool result)
    {
        DBG("[" << boost::sml::aux::get_type_name<SM>() << "][guard] " << " " << boost::sml::aux::get_type_name<TGuard>() << " " <<
                boost::sml::aux::get_type_name<TEvent>() << " " << (result ? "[OK]" : "[Reject]"));
        ignoreUnused(result);
    }

    template<class SM, class TAction, class TEvent>
    void log_action(const TAction&, const TEvent&)
    {
        DBG("[" << boost::sml::aux::get_type_name<SM>() << "][action] " << boost::sml::aux::get_type_name<TAction>() << " " <<
                boost::sml::aux::get_type_name<TEvent>());
    }

    template<class SM, class TSrcState, class TDstState>
    void log_state_change(const TSrcState& src, const TDstState& dst)
    {
        DBG("[" << boost::sml::aux::get_type_name<SM>() << "][transition] " << src.c_str() << " -> " << dst.c_str());
        ignoreUnused(src, dst);
    }
};

//======================================================================================================================
static inline const juce::Uuid ApiServiceUuid{"f3402bdc-d017-11e9-bb65-2a2ae2dbcce4"};
static inline const juce::Uuid ApiServiceCharactUuid{"f3402ea2-d017-11e9-bb65-2a2ae2dbcce4"};

static inline const juce::Uuid MidiServiceUuid{"03b80e5a-ede8-4b33-a751-6ce34ec4c700"};

struct SendFrame : public juce::Message {};

//======================================================================================================================
namespace fsm {
struct wave_api
{
    //==============================================================================================================
    class idle;
    class discovering_services;
    class discovering_service_details;
    class enabling_notifications;
    class receiving_data;

    //==============================================================================================================
    struct peripheral_state_updated { const ble::Peripheral& peripheral; };
    struct services_discovered { const ble::Peripheral& peripheral; };
    struct service_details_discovered { const ble::Service& service; };
    struct notifications_enabled { ble::Characteristic charact; };

    auto operator()() const noexcept
    {
        namespace sml = boost::sml;
        using namespace sml;

        //==============================================================================================================
        const auto enable = [](const juce::Uuid& uuid, ble::Characteristic::NotificationType type)
        {
            return [uuid, type](const auto& e)
            {
                auto charact = e.service.characteristic(uuid);

                if (charact)
                    e.service.enableNotifications(*charact, type);
                else
                    DBG("Characteristic not found: " << uuid.toDashedString());
            };
        };

        const auto discover_services = [](const peripheral_state_updated&, std::optional<ble::Peripheral>& p)
        {
            if (p.has_value())
                p->discoverServices();
        };

        const auto discover_service_details = [](const juce::Uuid& uuid)
        {
            return [uuid](const auto& e)
            {
                auto service = e.peripheral.service(uuid);

                if (service)
                    service->discoverDetails();
                else
                    DBG("Service not found: " << uuid.toDashedString());
            };
        };

        const auto reset_peripheral = [](std::optional<ble::Peripheral>& p) { p = std::nullopt; };

        const auto start_scan = [](ble::Adapter& ble) { ble.startScan({}); };
        const auto stop_scan  = [](ble::Adapter& ble) { ble.stopScan(); };

        const auto send_led_frame = [](juce::MessageListener& l) { l.postMessage(new SendFrame()); };

        //==============================================================================================================
        const auto is_connected    = [](const auto& e) { return e.peripheral.state() == ble::Peripheral::State::Connected; };
        const auto is_disconnected = [](const auto& e) { return e.peripheral.state() == ble::Peripheral::State::Disconnected; };

        const auto has_wave_api_service = [](const services_discovered& e)
        {
            const auto s = e.peripheral.services();
            return std::find_if(s.cbegin(), s.cend(), [](const auto& s) { return s.uuid() == ApiServiceUuid; }) != s.cend();
        };

        const auto is_wave_api_service = [](const service_details_discovered& e) { return e.service.uuid() == ApiServiceUuid; };
        const auto is_wave_api_charact = [](const notifications_enabled& e) { return e.charact.uuid() == ApiServiceCharactUuid; };

        return make_transition_table(
                *state<idle> + sml::on_entry<_> / reset_peripheral,
                state<idle> + event<peripheral_state_updated>[is_connected] / (stop_scan, discover_services) = state<discovering_services>,

                state<discovering_services> + event<services_discovered>[has_wave_api_service] / discover_service_details(ApiServiceUuid)                                = state<discovering_service_details>,
                state<discovering_service_details> + event<service_details_discovered>[is_wave_api_service] / enable(ApiServiceCharactUuid, ble::Characteristic::Notify) = state<enabling_notifications>,
                state<enabling_notifications> + event<notifications_enabled>[is_wave_api_charact] / send_led_frame                                                       = state<receiving_data>,
                state<receiving_data> + event<peripheral_state_updated>[is_disconnected] / start_scan                                                                    = state<idle>

        );
    }
};
} // namespace fsm
