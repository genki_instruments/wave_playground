#include "TimeSeriesGraph.h"

#include <utility>

TimeSeriesGraph::TimeSeriesGraph(int dimensions, double y_axis_min, double y_axis_max, String title) :
        plotTypes(dimensions),
        data(dimensions, std::deque<double>(max_samples)),
        min_y(y_axis_min),
        max_y(y_axis_max),
        plot_title(std::move(title)),
        paths(dimensions)
{
// startTimerHz(60);
}

void TimeSeriesGraph::append(const std::vector<double>& sample)
{
    auto r = getLocalBounds().reduced(10);

    auto x = r.getX();
    auto y = r.getY();
    auto width = r.getWidth();
    auto height = r.getHeight();

    auto pixels_per_unit = height / (max_y - min_y);

    auto zero_x = x + 45;
    auto zero_y = y + max_y * pixels_per_unit;

    min_y = 10e5;
    max_y = -10e5;

    for (auto i = 0; i < data.size(); ++i) {
        if (data[i].size() == max_samples) {
            data[i].pop_front();
        }

        if (std::isnan(sample[i]))
            break;

        data[i].push_back(sample[i]);

        const auto res = std::minmax_element(data[i].cbegin(), data[i].cend());
        min_y = std::min(min_y, *res.first);
        max_y = std::max(max_y, *res.second);

        paths[i].clear();
        if (std::isnan(data[i][0]))
            break;
        paths[i].startNewSubPath(zero_x, zero_y - data[i][0] * pixels_per_unit);

        for (auto j = 0; j < max_samples; ++j) {
            paths[i].lineTo(zero_x + j * width / max_samples, zero_y - data[i][j] * pixels_per_unit);
        }
    }
}

void TimeSeriesGraph::append(const double sample)
{
    jassert(data.size() == 1);

    std::vector<double> v{sample};
    append(v);
}

void TimeSeriesGraph::paint(Graphics& g)
{
    g.setColour(Colours::white);
    g.fillRect(getLocalBounds());

    auto r = getLocalBounds().reduced(10);

    auto x = r.getX();
    auto y = r.getY();
    auto width = r.getWidth();
    auto height = r.getHeight();

    auto pixels_per_unit = height / (max_y - min_y);

    auto zero_x = x + 45;
    auto zero_y = y + max_y * pixels_per_unit;

    // Draw axes
    Path axis_x, axis_y;
    axis_x.startNewSubPath(x, zero_y);
    axis_x.lineTo(x + width, zero_y);

    axis_y.startNewSubPath(zero_x, y);
    axis_y.lineTo(zero_x, y + height);

    g.setColour(Colours::black);
    g.strokePath(axis_x, PathStrokeType(1.0f));
    g.strokePath(axis_y, PathStrokeType(1.0f));

    const auto axis_increment = max_y / 4.0;

    // Draw numbers on axes
    if (axis_increment > 0.0f) {
        auto increment_pixels = jmax(10.0, axis_increment * pixels_per_unit);

        for (auto i = 0; i <= height; i += increment_pixels) {
            auto val = max_y - (i / increment_pixels) * axis_increment;

            // No need to label the origin
            if (val != 0.0f) {
                g.setFont(Font(10.0f));
                g.drawText(String(val, 2), Rectangle<int>(x, y + i - 5, 40, 10), Justification::right, true);

                Path p;
                p.startNewSubPath(zero_x - 2, y + i);
                p.lineTo(zero_x + 2, y + i);
                g.strokePath(p, PathStrokeType(1.0f));
            }
        }
    }

    // Plot data
    for (auto i = 0; i < data.size(); ++i) {
        if (plotTypes[i] == PlotType::Line) {
            g.setColour(colors[i]);
            g.strokePath(paths[i], PathStrokeType(1.0f));

        } else if (plotTypes[i] == PlotType::Points) {
            g.setColour(Colours::blue);

            for (auto j = 0; j < max_samples; ++j) {
                auto xx = zero_x + j * width / max_samples;
                auto yy = zero_y - data[i][j] * pixels_per_unit;

                if (data[i][j] != 0) {
                    g.fillEllipse(xx - PointSize / 2.0f, yy - PointSize / 2.0f, PointSize, PointSize);
                }
            }
        }
    }

    // Draw title
    if (plot_title.isNotEmpty()) {
        g.setFont(Font(12.0f));
        g.setColour(Colours::black);
        g.drawText(plot_title, getLocalBounds().removeFromTop(20), Justification::centred, true);
    }
}
