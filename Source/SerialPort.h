//
// Created by dingari on 12/10/18.
//

#ifndef WAVE_DASHBOARD_SERIALPORT_H
#define WAVE_DASHBOARD_SERIALPORT_H

#include "../JuceLibraryCode/JuceHeader.h"

#include <vector>

#include "asio.hpp"

#include "WavePublicTypes.h"
#include "ByteStuffing.h"

#if JUCE_WINDOWS
#include "atlstr.h"
#endif

namespace SerialPortMessage {
class Datastream : public juce::Message {
public:
    explicit Datastream(Wave::Datastream::SensorData  d, Wave::Datastream::MotionData m) : calData(d), motionData(m) {}

    Wave::Datastream::SensorData calData;
    Wave::Datastream::MotionData motionData;
};

class ButtonEvent : public juce::Message {
public:
    explicit ButtonEvent(Wave::ButtonEvent e) : event(e) {}

    Wave::ButtonEvent event{};
};
} // namespace SerialPortMessage

class SerialPort : public juce::Thread
{
public:
    explicit SerialPort(juce::MessageListener& l) : juce::Thread("Serial Port Thread"), listener(l) {}

    ~SerialPort() override { stopThread(1000); }

    void run() override
    {
        using namespace Wave::Api;

        DBG("Thread running: " << getThreadName());

        asio::streambuf serial_buffer{};

        // Read at least one (maybe partial) packet to ensure we are in sync.
        const auto bytes_read = asio::read_until(*serial, serial_buffer, PacketDelimiter);
        serial_buffer.consume(bytes_read);

        while (!threadShouldExit()) {
            try {
                const auto bytes = asio::read_until(*serial, serial_buffer, PacketDelimiter);

                std::array<uint8_t, 256> decoded{};
                jassert(bytes < decoded.size());

                const auto decoded_bytes = cobs_decode(asio::buffer_cast<const uint8_t*>(serial_buffer.data()), bytes, decoded.data());
                ignoreUnused(decoded_bytes);

                serial_buffer.consume(bytes);

                //======================================================================================================
                Query query{};
                std::memcpy(&query, decoded.data(), sizeof(query));

                switch (query.id) {
                    using Id = Query::Id;

//                    case Id::Datastream: {
//                        Wave::Datastream d{};
//                        std::memcpy(&d, decoded.data() +  sizeof(query), sizeof(d));
//
//                        listener.postMessage(new SerialPortMessage::Datastream(d.data, d.motionData));
//
//                        if (d.buttonEvent.id != Wave::ButtonId::Invalid) {
//                            listener.postMessage(new SerialPortMessage::ButtonEvent(d.buttonEvent));
//                        }
//
//                        break;
//                    }

                    case Id::BatteryStatus:
                    case Id::DeviceInfo:
                    case Id::ButtonEvent:
                    case Id::DeviceMode:
                    case Id::Identify:
                    case Id::MAX_VAL:
                    case Id::Datastream:
                    case Id::Recenter:
                    case Id::DisplayFrame:
                        break;
                }

                //======================================================================================================

            } catch (std::exception e) {
                DBG("Exception caught: " << e.what());
            }
        }
    }
    
    // TODO: Linux/Mac implementation
	juce::StringArray availablePorts() const 
	{ 
		StringArray ports{};

#if JUCE_LINUX
		ports.add("/dev/ttyUSB0");
        ports.add("/dev/ttyACM0");
        ports.add("/dev/ttyACM1");

#elif JUCE_WINDOWS
		CRegKey regkey{};

		if (!regkey.Open(HKEY_LOCAL_MACHINE, "HARDWARE\\DEVICEMAP\\SERIALCOMM", KEY_READ) == ERROR_SUCCESS) {
			DBG("Cannot access the Windows registry...", MB_OK);

			return ports;
		}

		DWORD idx = 0;
		std::array<char, 256> name{};
		ULONG str_size = name.size();

		while (RegEnumValue(regkey.m_hKey, idx, name.data(), &str_size, nullptr, nullptr, nullptr, nullptr) == ERROR_SUCCESS) {
			std::array<char, 256> value{};
			str_size = name.size();
			regkey.QueryStringValue(name.data(), value.data(), &str_size);

			const juce::String port(value.data());
			if (port.fromFirstOccurrenceOf("COM", false, false).getIntValue() >= 1) {
				ports.add(juce::String(port));
			}

			++idx;
			name.fill(0);
			str_size = name.size();
		}
#endif

		return ports;
	}

    void open(const juce::String& port, int baud)
    {
        DBG("Opening port: " << port << " with baud: " << String(baud));

        try {
            serial = std::make_unique<asio::serial_port>(io, port.toStdString());
            serial->set_option(asio::serial_port_base::baud_rate(baud));

            startThread();
        } catch (std::exception e) {
            DBG("Error opening port: " << e.what());
        }
    }

    void write(const uint8_t* data, size_t length)
    {
        std::array<uint8_t, 256> encoded{};

        const auto encoded_bytes = cobs_encode(data, length, encoded.data());
        const auto packet_bytes  = encoded_bytes + sizeof(PacketDelimiter);

        const auto bytes_written = asio::write(*serial, asio::buffer(encoded.data(), packet_bytes));

        std::printf("Wrote %lu bytes to serial\n", bytes_written);
    }

private:
    static constexpr uint8_t PacketDelimiter = 0x00;

    juce::MessageListener& listener;
    
    asio::io_service io{};
    std::unique_ptr<asio::serial_port> serial;
};


#endif //WAVE_DASHBOARD_SERIALPORT_H
