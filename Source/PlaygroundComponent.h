#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#include "RestRequest.h"

#include "FrameComponent.h"
#include "BluetoothFSM.h"
#include "OpenGLCube.h"
#include "TimeSeriesGraph.h"

#include "WavePublicTypes.h"
#include "Util.h"

namespace sml = boost::sml;

struct UpdateGoogleResult : public Message
{
    explicit UpdateGoogleResult(String s) : text(std::move(s)) {}

    String text;
};

struct UpdateGoogleStatus : public Message
{
    explicit UpdateGoogleStatus(String s) : text(std::move(s)) {}

    String text;
};

//======================================================================================================================
class PlaygroundComponent : public Component,
                            public MessageListener,
                            public ble::Adapter::Listener,
                            public ble::Peripheral::GattListener,
                            private Timer
{
public:
    //=================================================================================================================-
    PlaygroundComponent();

    ~PlaygroundComponent() override;

    //=================================================================================================================-
    void paint(Graphics&) override;

    void resized() override;

    void handleMessage(const Message&) override;

    //==================================================================================================================
    void stateUpdated() override
    {
        if (ble.state() == ble::Adapter::Idle)
        {
            if (const auto ps = ble.getConnectedPeripherals(MidiServiceUuid); !ps.empty())
                connect(ps.front());
            else
                ble.startScan({});
        }
    }

    void peripheralDiscovered(const ble::Peripheral& p) override
    {
        if (!isConnected() && p.name() == "Wave")
            connect(p);
    }

    void peripheralStateUpdated(const ble::Peripheral& p) override
    {
        sm.process_event(fsm::wave_api::peripheral_state_updated{p});
    }

    void peripheralRssiUpdated(const ble::Peripheral& p, double) override {}

    //=================================================================================================================-
    void onServicesDiscovered() override
    {
        sm.process_event(fsm::wave_api::services_discovered{*peripheral});
    }

    void onServiceDetailsDiscovered(const ble::Service& service) override
    {
        sm.process_event(fsm::wave_api::service_details_discovered{service});
    }

    void onValueChanged(const ble::Characteristic& charact, gsl::span<const gsl::byte> data) override
    {
        if (charact.uuid() == ApiServiceCharactUuid)
        {
            using Query = Wave::Api::Query;

            const auto[query, s_rem] = Util::unpack<Query>(data);

            switch (query.id)
            {
                using Id = Query::Id;

                case Id::Datastream:
                {
                    const auto d = Util::copy<Wave::Datastream>(s_rem);

                    MessageManagerLock lock{};
                    newData(d.data, d.motionData);
                    break;
                }

                case Id::ButtonEvent:
                {
                    const auto evt = Util::copy<Wave::ButtonEvent>(s_rem);

                    MessageManagerLock lock{};
                    handleButtonEvent(evt);
                    break;
                }

                case Id::BatteryStatus:
                {
                    const auto   bs   = Util::copy<Wave::BatteryStatus>(s_rem);
                    const String text = String("|   Battery: ") + String((int) bs.percentage) + String("%") +
                                        String(" - Last received: ") +
                                        Time::getCurrentTime().toString(false, true, true, true);

                    batteryLabel.setText(text, dontSendNotification);

                    break;
                }

                case Id::DeviceInfo:
                case Id::DeviceMode:
                case Id::Identify:
                case Id::Recenter:
                case Id::DisplayFrame:
                case Id::MAX_VAL:
                    break;
            }
        }
    }

    void onNotificationStateUpdated(const ble::Characteristic& charact) override
    {
        sm.process_event(fsm::wave_api::notifications_enabled{charact});
    }

private:
    //==================================================================================================================
    template<typename... Args>
    void sendRequest(const Wave::Api::Query::Id& id, const Args& ... args)
    {
        if (peripheral.has_value())
        {
            using namespace Wave::Api;

            const Query q{Query::Type::Request, id, (uint16_t) Util::byte_size(args...)};

            std::vector<gsl::byte> buf(Util::byte_size(q, args...));

            const auto s_buf = gsl::make_span(buf.data(), buf.data() + buf.size());
            Util::pack(gsl::as_writeable_bytes(s_buf), q, args...);

            peripheral
                    ->service(ApiServiceUuid)
                    ->characteristic(ApiServiceCharactUuid)
                    ->write(gsl::as_bytes(s_buf), true);
        }
        else
            jassertfalse;
    }

    void connect(const ble::Peripheral& p)
    {
        peripheral = p;
        ble.connect(*peripheral);
        peripheral->setListener(this);
    }

    bool isConnected() const { return !sm.is(sml::state<fsm::wave_api::idle>); }

    void newData(const Wave::Datastream::SensorData& d, const Wave::Datastream::MotionData& m);

    void handleButtonEvent(const Wave::ButtonEvent&);

    void processPaths();

    void timerCallback() override
    {
        if (sm.is(sml::state<fsm::wave_api::receiving_data>))
            sendRequest(Wave::Api::Query::Id::BatteryStatus);
    }

    //==================================================================================================================
    float prevTimestamp{};

    ble::Adapter                   ble;
    std::optional<ble::Peripheral> peripheral;

    LoggerFSM_juce logger{};

    sml::sm<fsm::wave_api, sml::logger<decltype(logger)>> sm;

    //==================================================================================================================
    TimeSeriesGraph accelGraph, gyroGraph, eulerGraph;

    OpenGLCube cube{};

    //==================================================================================================================
    Label          handwritingDemo{"", "Handwriting demo"};
    Rectangle<int> drawing_board;

    juce::Point<float> display_pos;
    bool               is_drawing = false;
    std::vector<Path>  paths;

    RestRequest request{"https://www.google.com"};
    Label       googleStatus{"", "Use Button C to process handwriting"}, googleResult;

    //==================================================================================================================
    Label sampleRateLabel{"", "Sample rate: "},
          batteryLabel{"", "|   Battery: "};

    FrameComponent led;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(PlaygroundComponent)
};
