/*
  ==============================================================================

    Utilities.h
    Created: 2 Oct 2017 12:14:45pm
    Author:  David Rowland

  ==============================================================================
*/

#pragma once

namespace drow
{

//==============================================================================
/** Iterates through a list of Components, calling a function on each.
    @param fn   The signature of the fn should be equivalent to "void fn (Component* c)"
*/
template<typename FunctionType>
inline void visitComponents (std::initializer_list<Component*> comps, FunctionType&& fn)
{
    std::for_each (std::begin (comps), std::end (comps), fn);
}

template<typename FunctionType>
inline void visitComponents (std::initializer_list<ChangeBroadcaster*> comps, FunctionType&& fn)
{
    std::for_each (std::begin (comps), std::end (comps), fn);
}

template<typename C, typename FunctionType>
inline void visitComponents (std::initializer_list<C*> comps, FunctionType&& fn)
{
    std::for_each (std::begin (comps), std::end (comps), fn);
}

//==============================================================================
/** Adds and makes visible any number of Components to a parent. */
inline void addAndMakeVisible (Component& parent, Array<Component*> comps)
{
    std::for_each (std::begin (comps), std::end (comps),
                   [&parent] (Component* c) { parent.addAndMakeVisible (c); });
}

/** Adds and makes visible any number of Components to a parent. */
inline void addAndMakeVisible (Component& parent, std::initializer_list<Component*> comps)
{
    std::for_each (std::begin (comps), std::end (comps),
                   [&parent] (Component* c) { parent.addAndMakeVisible (c); });
}
}
