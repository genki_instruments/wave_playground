#ifndef WAVEPUBLICTYPES_H
#define WAVEPUBLICTYPES_H

#include <cstdint>
#include <array>
#include <tuple>
#include <cstring>

namespace Wave {
//======================================================================================================================
enum class DeviceMode : std::uint8_t
{
    Preset    = 100,
    Softwave  = 101,
    Wavefront = 102,
    Api       = 103,
};

namespace Api {
//======================================================================================================================
enum class QueryType : std::uint8_t { Request = 1, Response, Stream, MAX_VAL };

struct Query
{
    using Type = QueryType;

    enum class Id : std::uint8_t
    {
        Datastream = 1,
        BatteryStatus,
        DeviceInfo,
        ButtonEvent,
        DeviceMode,
        Identify,
        Recenter,
        DisplayFrame,
        MAX_VAL
    };

    const Type     type{};
    const Id       id{};
    const uint16_t payload_size{};
};

} // namespace Api

//======================================================================================================================
template<size_t max_size>
class String
{
private:
    std::array<char, max_size + 1> chars{};

public:
    String() = default;

    String(const char* s) { std::memcpy(chars.data(), s, strnlen(s, max_size)); }

    [[nodiscard]] const char* c_str() const { return chars.data(); }

    [[nodiscard]] size_t length() const { return strnlen(chars.data(), max_size); }

    bool operator==(const String& rhs) const
    {
        const auto len = rhs.length();
        return len == length() && std::strncmp(chars.data(), rhs.chars.data(), len) == 0;
    }

    bool operator!=(const String& rhs) const { return !(*this == rhs); }
};

//======================================================================================================================
// Using semantic versioning format
struct Version
{
    uint8_t major, minor, patch;

    [[nodiscard]] uint32_t as_number() const { return (uint32_t) ((major << 16u) | (minor << 8u) | (patch << 0u)); }

    static Version from_number(uint32_t n)
    {
        return {
                .major = (uint8_t) (n >> 16u),
                .minor = (uint8_t) (n >> 8u),
                .patch = (uint8_t) (n >> 0u),
        };
    }

    bool operator!=(const Version& other) const { return (patch != other.patch) || (minor != other.minor) || (major != other.major); }
    bool operator==(const Version& other) const { return !(*this != other); }
    bool operator<(const Version& other) const
    {
        return (major < other.major)
               || (major == other.major && minor < other.minor)
               || (major == other.major && minor == other.minor && patch < other.patch);
    }
};

struct MacAddress
{
    static constexpr int      Size = 6;
    std::array<uint8_t, Size> data;

    MacAddress() = default;

    MacAddress(const uint8_t a[Size]) : data({a[0], a[1], a[2], a[3], a[4], a[5]}) {}

    bool operator==(const MacAddress& other) { return data == other.data; }
};

namespace Constants {
static constexpr uint8_t SerialNumberLength = 16;
static constexpr uint8_t BoardVersionLength = 8;
}

using SerialNumber = String<Constants::SerialNumberLength>;
using BoardVersion = String<Constants::BoardVersionLength>;

struct DeviceInfo
{
    Version      firmware_version;
    BoardVersion board_version;

    MacAddress bluetooth_address;

    SerialNumber serial_number;
};

//======================================================================================================================
enum class ButtonId : std::uint8_t { A, B, C, D };

enum class ButtonAction : std::uint8_t
{
    Up,
    Down,
    Long,
    LongUp,
    ExtraLong,
    ExtraLongUp,
    Click,
    DoubleClick,
};

struct ButtonEvent
{
    ButtonId     id;
    ButtonAction action;
    float        timestamp{};

    bool operator==(const ButtonEvent& rhs) const { return id == rhs.id && action == rhs.action; }
    bool operator<(const ButtonEvent& rhs) const { return std::tie(id, action) < std::tie(rhs.id, rhs.action); }
};

struct BatteryStatus { float voltage, percentage; bool is_charging; };

struct Datastream
{
    struct SensorData
    {
        std::array<float, 3> gyro, accel, mag;

        float timestamp;
    } data;

    struct MotionData
    {
        std::array<float, 4> rawPose, currentPose;
        std::array<float, 3> euler;

        struct Peak
        {
            bool  detected;
            float normVelocity;
        };

        Peak tap;

        float timestamp;
    } motionData;
};

//======================================================================================================================
struct MidiEvent
{
    uint8_t cc{}, value{};
};

//======================================================================================================================
struct Pixel
{
    uint8_t row{}, column{};
};

//======================================================================================================================
template<typename Type>
struct Range
{
    Type min, max;

    constexpr Range<Type> inverted() const { return {max, min}; }

    template<typename NewType>
    constexpr Range<NewType> to() const { return {static_cast<NewType>(min), static_cast<NewType>(max)}; }
};

using Rangef = Range<float>;
using Ranged = Range<double>;
using Rangei = Range<int>;

enum class Orientation : uint8_t { Left, Right };

} // namespace Wave

#endif  // WAVEPUBLICTYPES_H
