#include "../JuceLibraryCode/JuceHeader.h"

#include <deque>

class TimeSeriesGraph : public Component {
public:
    TimeSeriesGraph(int dimensions = 1, double y_axis_min = 0, double y_axis_max = 0, juce::String title = "");

    ~TimeSeriesGraph() override = default;

    void append(const std::vector<double>& sample);

    void append(double sample);

    void setTitle(const juce::String& title)
    {
        plot_title = title;
    }

    void paint(Graphics& g) override;

    enum class PlotType {
        Line,
        Points,
    };

    void setPlotType(int index, PlotType type)
    {
//        jassert(index < plotTypes.size());

        plotTypes[index] = type;
    }

private:
    static constexpr float PointSize = 5.0f;

    std::vector<juce::Colour> colors = {juce::Colours::red, juce::Colours::green, juce::Colours::blue,
                                        juce::Colours::grey};
    std::vector<PlotType> plotTypes = {PlotType::Line, PlotType::Line, PlotType::Line, PlotType::Line};

    std::size_t max_samples = 100;
    std::vector<std::deque<double>> data;

    double min_y, max_y;

    juce::String plot_title;

    std::vector<Path> paths;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TimeSeriesGraph)
};