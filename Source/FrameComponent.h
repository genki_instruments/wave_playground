/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "WavePublicTypes.h"

class Pixel : public Component
{
public:
    void setSelected(bool s) { selected = s; }

    void paint(Graphics& g) override
    {
        g.setColour(selected ? Colours::cyan : Colours::white);
        g.fillEllipse(getLocalBounds().toFloat());
    }

    void resized() override {}

private:
    bool selected = false;
};

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class FrameComponent : public Component
{
public:
    //==============================================================================
    FrameComponent() : slider(Slider::LinearHorizontal, Slider::TextBoxLeft)
    {
        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                addAndMakeVisible(pixels[i][j]);
                pixels[i][j].setAlpha(0);
            }
        }

        addAndMakeVisible(slider);
        addAndMakeVisible(resetButton);
        addAndMakeVisible(exportButton);

        slider.setRange(Range<double>(0, 255), 1);
        slider.setValue(0);
        slider.onValueChange = [this]
        {
            if (selectedPixel != nullptr)
            {
                selectedPixel->setAlpha(jmap(slider.getValue(), slider.getRange().getStart(), slider.getRange().getEnd(), 0.0, 1.0));

                repaint();
            }
        };

        exportButton.onClick = [this]
        {
            using namespace Wave;

            const auto frame = getFrame();

            std::printf("{");
            for (int i = 0; i < M; ++i)
            {
                std::printf("{");

                for (int j = 0; j < N; ++j)
                    std::printf(j == N - 1 ? "%u" : "%u, ", frame[i][j]);

                std::printf("},\n");
            }
            std::printf("}\n");
        };

        resetButton.onClick = [this]
        {
            for (int i = 0; i < M; ++i)
                for (int j = 0; j < N; ++j)
                    pixels[i][j].setAlpha(0);
        };

        addMouseListener(this, true);

        setSize(400, 400);
    }

    ~FrameComponent() override = default;

    //==============================================================================
    void paint(Graphics&) override {}

    void resized() override
    {
        auto       r          = getLocalBounds();
        auto       bottom_row = r.removeFromBottom(30);
        const auto bottom_w   = r.proportionOfWidth(1 / 3.0);
        slider.setBounds(bottom_row.removeFromLeft(bottom_w).reduced(4));
        resetButton.setBounds(bottom_row.removeFromLeft(bottom_w).reduced(4));
        exportButton.setBounds(bottom_row.removeFromLeft(bottom_w).reduced(4));

        const auto h = r.proportionOfHeight(1 / (double) M);
        const auto w = r.proportionOfWidth(1 / (double) N);

        for (int i = 0; i < M; ++i)
        {
            auto row = r.removeFromTop(h);

            for (int j = 0; j < N; ++j)
            {
                pixels[i][j].setBounds(row.removeFromLeft(w).reduced(4));
            }
        }
    }

    void mouseDown(const MouseEvent& e) override
    {
        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                const auto mouse_in_parent = e.getEventRelativeTo(this).getPosition();
                const bool focused         = pixels[i][j].getBoundsInParent().contains(mouse_in_parent);

                if (focused)
                {
                    DBG("Pixel selected: " << mouse_in_parent.toString());
                    selectedPixel = &pixels[i][j];
                    slider.setValue(jmap((double) selectedPixel->getAlpha(), slider.getRange().getStart(), slider.getRange().getEnd()));
                }

                pixels[i][j].setSelected(focused);
            }
        }

        repaint();
    }

    void mouseDrag(const MouseEvent& event) override
    {
        const auto alpha = jmap((double) event.getDistanceFromDragStartY(), (double) pixels[0][0].getHeight(), 1.0, 0.0, 1.0);

        if (selectedPixel != nullptr)
        {
            selectedPixel->setAlpha(alpha);
            slider.setValue(jmap((double) alpha, slider.getRange().getStart(), slider.getRange().getEnd()));
        }
    }

    void mouseUp(const MouseEvent& event) override { if (onChange) onChange(); }

    static constexpr int M = 5;
    static constexpr int N = 9;

    [[nodiscard]] std::array<std::array<uint8_t, N>, M> getFrame() const
    {
        std::array<std::array<uint8_t, N>, M> frame{};

        for (int i = 0; i < M; ++i)
            for (int j = 0; j < N; ++j)
                frame[i][j] = jmap((double) pixels[i][j].getAlpha(), slider.getRange().getStart(), slider.getRange().getEnd());

        return frame;
    }

    void setFrame(const std::array<std::array<uint8_t, N>, M>& frame)
    {
        for (int i = 0; i < M; ++i)
            for (int j = 0; j < N; ++j)
                pixels[i][j].setAlpha(jmap((double) frame[i][j], slider.getRange().getStart(), slider.getRange().getEnd(), 0.0, 1.0));
    };

    std::function<void()> onChange;

private:
    std::array<std::array<Pixel, N>, M> pixels;

    Pixel* selectedPixel = nullptr;

    Slider slider;

    TextButton resetButton{"Reset"}, exportButton{"Export frame"};

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (FrameComponent)
};
