//
// Created by Genki Instruments on 2/3/20.
//

#pragma once

#include <cassert>
#include <utility>

namespace Util {

template<std::size_t num>
auto split(const gsl::span<const gsl::byte>& s) { return std::make_pair(s.first(num), s.subspan(num)); }

template<typename T>
T copy(gsl::span<const gsl::byte> s)
{
    assert(sizeof(T) == s.size());

    T t;
    std::memcpy(&t, s.data(), sizeof(t));
    return t;
}

constexpr size_t byte_size() { return 0; }

template<typename T, typename... Ts>
constexpr size_t byte_size(const T& t, const Ts& ... ts)
{
    if constexpr (std::is_standard_layout<T>::value)
    {
        return sizeof(t) + byte_size(ts...);
    }
    else
    {
        return (size_t) t.size() + byte_size(ts...);
    }
}

constexpr auto pack(gsl::span<gsl::byte> s) { return s; }

template<typename T, typename... Ts>
constexpr auto pack(gsl::span<gsl::byte> dest, const T& t, const Ts& ... ts)
{
    if constexpr (std::is_standard_layout<T>::value)
    {
        const auto s = gsl::as_bytes(gsl::make_span(&t, 1));
        std::copy(s.cbegin(), s.cend(), dest.begin());

        return pack(dest.subspan(s.size()), ts...);
    }
    else
    {
        // NOTE: This assumes T is a span type
        const auto s = gsl::as_bytes(t);
        std::copy(s.cbegin(), s.cend(), dest.begin());

        return pack(dest.subspan(s.size()), ts...);
    }
}

constexpr auto unpack(gsl::span<const gsl::byte> s) { return std::make_tuple(s); }

template<typename T, typename... Ts>
constexpr auto unpack(gsl::span<const gsl::byte> s)
{
    if constexpr (std::is_standard_layout<T>::value)
    {
        const auto[left, right] = split<sizeof(T)>(s);
        const auto current = copy<T>(left);

        if constexpr (sizeof...(Ts) > 0)
            return std::tuple_cat(std::make_tuple(current), unpack<Ts...>(right));
        else
            return std::tuple_cat(std::make_tuple(current), unpack(right));
    }
}
}
